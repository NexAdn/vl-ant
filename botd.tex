\chapter{Buzzword Of The Day}

In den Vorlesungen wird ein „Buzzword“ bzw. Begriff aus der Welt der Netzwerktechnik genannt.
Dieser Begriff ist von den Hörenden zu recherchieren und wird in der anschließenden Vorlesung diskutiert.
Hier sind einige Begriffe samt Diskussionen und zusammengetragen.

\section[Cut-Through-Switching]{Cut-Through Switching \cite{noauthor_cut-through_2024, noauthor_switching_nodate}}

Cut-Through Switching vermeidet Latenz,
indem ein Paket bereits an den ausgenden Port verschickt wird,
sobald die Zieladresse und der ausgehende Port bestimmt werden konnten.
Das Paket kann also weitergeleitet werden, noch bevor es vollständig empfangen wurde.

Cut-Through Switching erfordert,
dass die Linkgeschwindigkeit des ausgehenden Ports mindestens so groß ist wie die des eingehenden Ports,
da sonst das Paket wieder gepuffert werden muss.
Ein weiteres Problem ergibt sich dadurch,
dass die Checksumme des Ethernet Frames nicht geprüft werden kann,
bevor das Paket verschickt wird
(einerseits, da die Payload zu diesem Zeitpunkt noch nicht ganz bekannt ist,
andererseits, da die Checksumme erst am Ende des Ethernet Frames steht).
Daher leitet ein Cut-Through Switch auch beschädigte Frames weiter,
die ein Store-and-Forward Switch verwerfen würde.

Cut-Through Switching kann auch in einer adaptiven Variante betrieben werden.
In diesem Fall wird es nur benutzt, solange Fehler am Link nur selten passieren.
Steigt hingegen die Fehlerrate am Port,
wird auf Store and Forward umgeschaltet,
um unnötiges Weiterleiten von Paketen zu vermeiden.

Eine andere Erweiterung ist Fragment-Free-Cut-Through,
bei der nur die ersten 64~Bytes eines Ethernet Frames geprüft werden.
Dies beruht auf der Erfahrung, dass Framefehler meistens nur innerhalb der
ersten 64~Byte auftreten.

In einem RZ-Setup ist üblicherweise für jedes Rack ein Top-Of-Rack-Switch verbaut,
an dem die Server des Racks angeschlossen sind und der das Rack
mit dem Backbone verbindet.
Das Problem dabei ist, dass Pakete fast immer im Top-Of-Rack-Switch gepuffert werden müssen,
weil sie von den Servern zeitgleich Pakete empfangen.
In so einem Fall bringt Cut-Through also nichts.
Änlich bringen die Latengewinne im Nanosekundenbereich selbst für latenzkritische Anwendungen
(wie bspw. PTP) nicht viel, da die Latenzen an den Endsystemen meist deutlich größer sind
und somit die Gewinne durch Cut-Through-Switching zunichte machen.

% \section[Hairpin Turn]{Hairpin Turn \cite{brainard_hairpinning_2021,noauthor_hairpinning_nodate}}
% 
% Hierbei wird anstelle einer direkte(re)n Route
% Traffic über einen Zwischen-Hop geleitet, der bspw. NAT oder Firewall-Regeln anwendet.
% Eingesetzt wird dies u.\,A.~für VoIP,
% um Telefon-Traffic, wo der VoIP über einen PBX statt direkt zur Gegenstation geleitet wird.
% Andere Anwendungsfälle umfassen Szenarien,
% in denen der Zugriff auf einen Server im gleichen Netzwerk stattfindet,
% wo aber auf dessen NAT-IP zugegriffen wird.
% Hier muss zunächst der Traffic zum NAT-GW geschickt werden,
% um die öffentliche Adresse in die private umzuwandeln.
% Ein weiteres Szenario ist die Bereitstellung von Internet-Uplink
% für mehrere Außenstellen eines Betriebs,
% bei dem sämtlicher Traffic zunächst über die firmeneigene Firewall geleitet werden soll.
% Dabei wird sämtlicher Traffic der Außenstellen statt direkt zur Gegenstelle
% immer zunächst zur Firewall geleitet und
% kommt erst von dort ins Internet.
% 
% Das Hauptproblem mit Hairpinning ist, dass der Traffic nicht den direkten Weg geht.
% Dies führt grundsätzlich zu erhöhter Latenz und
% durch die Bündelung sämtlichen Traffics an einer Stelle auch zu mehr Congestion.
% Dies wiederum wirkt sich auf die Performance der Netzwerkverbindungen aus.
% So kann es durch erhöhte Latenz und erhöhte Wahrscheinlichkeit von Paketverlust
% zu schlechtem TCP-Durchsatz führen.

\section{Hairpin Turn}
\label{sec:hairpin}

Es ist teilweise notwendig (z.\,B.~bei VLAN-Routing, Routing im IX),
dass ein Router ein Paket an den gleichen Port zurückzuschicken,
an dem das Paket ankam.
Bei Ethernet ist dies nicht möglich,
da sonst durch Backward Learning das Paket im Kreis geschickt wird
(wird zwischen zwei Switchen direkt hin und hergeschickt).
Schaltet man jedoch Backward Learning aus,
kann ein eingehendes Paket auch bei Ethernet direkt zurückgeschickt werden.
Dies kann beispielsweise im Cloud-Umfeld genutzt werden,
um Traffic-Policies, ACLs, u.\,Ä.~auf Switch-Ebene auch für Traffic durchzusetzen,
der zwischen zwei VMs auf dem gleichen VM-Host läuft.

Eine klare Lektion hierbei ist,
dass ein Switch nicht nur von einem Port an andere (vom Input Port ungleiche) Ports sendet,
sondern auch wieder zurück an den Port selbst.

\section[Explicit Congestion Notification]{Explicit Congestion Notification \cite{fairhurst_rfc_2017}}\label{sec:ecn}

Explicit Congestion Notification (ECN) ermöglicht es Routern und Switchen,
den Endpunkten einer Transportverbindung zu signalisieren,
wenn eine Queue Congestion erfährt.
Dazu setzen Endpunkte ein Bit in den zwei für ECN vorgesehenen Bits im IP-Header.
Erfährt nun ein Router/Switch Congestion,
werden beide Bits gesetzt, um Congestion anzuzeigen.
Die Gegenseite erkennt diese Benachrichtigung und gibt sie in ihrer Antwort
an den Sender (z.\,B.~im TCP ACK als Flag) wider.
Der Sender kann daraufhin Maßnahmen ergreifen, um Congestion vorzubeugen.

ECN erfordert Unterstützung sowohl an beiden Endpunkten der Verbindung
als auch bei den Geräten auf dem Pfad zwischen beiden Endpunkten.
Zwar ist es prinzipiell möglich,
dass innere Knoten des Pfades kein ECN unterstützen,
jedoch dürfen diese Pakete mit gesetzen ECN-Bits nicht verwerfen
und auch ihre Routing-Entscheidungen dadurch nicht verändern
(was möglich ist, wenn diese Bits anders interpretiert werden).
Außerdem funktioniert ECN nur,
wenn die inneren Knoten des Pfades auch tatsächlich bei bevorstehender Congestion
(welche i.\,d.\,R.~durch einen AQM-Algorithmus erkannt werden muss)
die beiden ECN-Bits setzen.
Passiert dies nicht, muss Congestion weiterhin über herkömmliche Methoden
wie Packet Loss erkannt werden,
was je nach Situation zu größeren Verzögerungen bei der Erkennung von Congestion führt
(z.\,B.~bei Tail Drop am Ende eines Bursts)
signifikante Verschlechterungen der Verbindungsqualität zur Folge hat
(z.\,B.~bei unzuverlässiger Videoübertragung über RTP,
bei denen es keine Retransmissions gibt).

Indem ECN den Sender über Congestion informiert, bevor Pakete gedroppt werden müssen,
kann das Sendeverhalten (die Senderate) angepasst werden,
ohne Daten zu verlieren.
Gerade bei unzuverlässigen Übertragungen wie z.\,B.~Echtzeit-Video
kann dies die Verbindungsqualität verbessern.

Ein Problem bei der Einführung von ECN ist Abwärtskompatibilität.
Die für ECN genutzten Bits des IP-Headers waren zwar zuvor reservierte Bits,
wurden jedoch auch schon für andere Zwecke (z.\,B. Routing-Entscheidungen) genutzt.
Dies bedeutet, dass ein Router gesetzte ECN-Bits fehlinterpretieren kann
und bspw. Pakete mit gesetzten ECN-Bits anders routet.
Dies wiederum kann zu Reordering führen,
was die Performance der Verbindung einschränkt.
ECN ist außerdem nur dann effektiv, wenn die Knoten im Pfad,
die Congestion erfahren, auch ECN-fähig sind.

Das andere Problem bei der Einführung von ECN ist, dass AQM quasi Pflicht für Knoten ist.
Ohne AQM wird auch keine Entscheidung getroffen,
wann der Knoten Congestion erfährt und wann er eine ECN versenden soll.
Somit kann insbesondere ältere Hardware oder
Hardware, die normales FIFO-Queueing mit Tail-Drop benutzt, keine ECN versenden.

Zuletzt können Probleme entstehen, wenn einigen Knoten ECN-fähig sind
und deren AQM-Algorithmus die ECN-Bits setzt, statt Pakete zu droppen,
ein nachfolgender Knoten aber (aufgrund irgendwelcher Policies zur Bereinigung von Headern)
die ECN-Bits auf 0 setzt (sprich, ECN ausschaltet).
Dann würde der Sender sein Sendeverhalten nicht anpassen,
was möglicherweise zu Problemen im AQM-Algorithmus führen kann.

\section{L4S}

L4S ist die \emph{Low Latency, Low Loss and Scalable Throughput} Architektur,
ein im experimentellen RFC 9330 \cite{briscoe_low_2023} definiertes Verfahren,
das als Ersatz zu herkömmlicher TCP Congestion Control dienen
und dabei unter Wahrung geringer Latenz und geringem Paketverlust
besseren Durchsatz liefern soll.
Dazu werden ECNs (\autoref{sec:ecn}) neu interpretiert,
um häufigere Informationen über Queue-Füllstände
noch vor dem tatsächlichen Auftreten von Congestion per ECN zu übermitteln.
So sollen Sender frühzeitig erfahren,
wenn eine Queue sich zu füllen beginnt,
um ihre Sendeleistung entsprechend anzupassen und
damit einen sanfteren Abfall der Senderate als
bei herkömmlicherer TCP Congestion Control zu erzielen.
Indem die Senderate leicht zurückgefahren wird,
noch bevor die Queues vollaufen,
soll der Fall vermieden werden,
dass durch eine volle Queue überhaupt Pakete gedroppt werden müssen,
womit im Idealfall maximaler Durchsatz ohne Paketverlust erzielt werden soll.

Kernproblem mit L4S ist die teilweise
fehlende Abwärtskompatibilität zu herkömmlichem TCP
sowie die benötigte Unterstützung in Switchen.
So erfordert L4S, dass möglichst viele Switche ECN-fähig sind
und dedizierte Queues für L4S bzw. Flow-Queueing unterstützen.
Dies macht eine Umstellung nur schwer möglich.
Ein weiteres Problem bei Alt-Hardware auf dem Pfad ist
die Wiederverwendung einer bereits für andere Zwecke vergebenen ECN-Markierung,
um L4S-Unterstützung anzuzeigen.
Damit besteht das Problem, dass alte Hardware L4S-Flows nicht so behandelt
wie diese es erwarten,
was zu Problemen mit der ordnungsgemäßen Funktion der Congestion Control führen kann.

\section{Intent Based Networking}

Bei SDN führt die Hardware einfache Kommandos aus,
die von einem Controller konfiguriert werden.
Bei Intent Based Networking soll der Programmierer gegenüber dem IBN-System
nur seine Absichten (z.\,B. „Computer A soll möglichst schnell mit Computer B reden“)
und das Netzwerk konfiguriert sich selbst automatisch so,
dass dies umgesetzt werden.
Diese Technik klappt allerdings nur so semi-gut.

\section[Zero Trust]{Zero Trust \cite{noauthor_zero_2024, bsi_zero_nodate}}

Zero Trust ist ein Architekturprinzip,
bei dem die Grundannahme ist,
dass kein Teil des Netzes als vertrauenswürdig angesehen wird.
So wird versucht,
dass jeder Kommunikationsvorgang authentifiziert und verschlüsselt ist,
um Vertraulichkeit und Integrität sicherzustellen.
Dies hat eine bessere Angriffsprävention zum Ziel,
da Angreifern sehr viele und hohe Hürden gelegt werden sollen,
die einen Angriff unwahrscheinlicher machen.

Weitere Implikationen aus dem Zero-Trust-Prinzip erfordern,
dass alle Geräte, die im Netzwerk mitwirken,
zunächst klassifiziert und ihre Schlüssel freigeschaltet werden müssen.
Für jedes Gerät muss festgelegt werden,
welche (minimalen) Berechtigungen das Gerät erhält,
um gerade die Dinge im Netzwerk durchführen zu können,
die es gemäß Klassifikation des Administrators können soll.

\section{SD-WAN}

\emph{SD-WAN}\index{SD-WAN} versucht,
mit Tunneltechnologien wie IPsec SDN über Weitverkehrsnetze wie das Internet zu machen.
An den Edges der LAN stehen dazu SD-WAN Gateways,
die Tunnelverbindungen zwischen den LANs aufbauen.
Diese werden automatisch verwaltet,
um den Traffic über mehrere Uplinks zu verteilen.
So können die genutzten Uplinks umgeschaltet werden, wenn einer ausfällt,
oder ein kostengünstigerer Uplink genutzt werden kann.
Andere Kriterien können sein,
ob Latenzmessungen ergeben,
dass bestimmte Uplinks bessere Latenzen haben.
Es kann weiterhin auch eingerichtet werden,
dass normaler Internet-Traffic direkt ins Internet
statt ins Firmen-VPN geleitet wird.

SD-WAN setzt üblicherweise kein OpenFlow ein,
da aufgrund der hohen Latenzen und der Unzuverlässigkeit
das Schreiben von Regeln u.\,Ä. Vorgänge nicht sicher funktioniert.
Stattdessen werden üblicherweise (untereinander inkompatible)
proprietäre Lösungen eingesetzt.

Hauptziel von SD-WAN sind Kosteneinsparungen,
weil auch kostengünstige Consumer-Uplinks genutzt werden können,
statt teure Unternehmens-Anschlüsse zu benutzen.
Probleme können trotzdem auftreten,
wenn alle Uplinks über die gleiche physische Infrastruktur laufen
und bspw. aufgrund eines Stromausfalls ein Teil dieser Infrastruktur ausfällt.
Bei Consumer-Anschlüssen gibt es keine Garantien über physische Redundanz
oder schnelle Behebung von Problemen.
Daher kann die Ausfallsicherheit durch SD-WAN auch leiden.

\section{SASE}

Bei herkömmlicher SD-WAN-Logik sind mehrere Netze über Links verbunden.
An den Edge-Knoten läuft dafür bereits Logik,
um das Routing zu steuern.
Dabei war bereits Security-as-a-Service
(Sicherheitsdienste werden von einem Cloud-Rechenzentrum angeboten) ein Thema.
Dabei wurde der Traffic erst einmal zum RZ geschickt,
bevor er das Internet erreicht.
Dast schadet natürlich der Performance.

\emph{SASE}\index{SASE} verschiebt die Sicherheitsdienste wieder an die lokalen PoPs.
Auf VMs o.\,Ä. werden die Sicherheitsdienste ausgeführt
und nur noch von der Cloud zentral gesteuert.
Damit kombiniert SASE SDN und NFV im WAN-Bereich.

\section[Hyper-Converged Storage]{Hyper-Converged Storage \cite{noauthor_converged_2023,ehnes_was_2020}}

\emph{Converged Storage}\index{Converged Storage} ist eine Storage-Architektur,
bei der Rechen- und Speicherresourcen in einer Einheit verbunden sind.
Dies soll die Performance der laufenden Anwendungen verbessern.
Es wird dabei der Fakt ausgenutzt, dass heutige Industrieserver
freie Kapazitäten haben,
neben Rechenleistung auch Speicherlast zu tragen.
Statt separate Storage- und Compute-Server zu haben,
werden beie Dienste in der gleichen Hardware bereitgestellt,
was wiederum Rackspace spart.

\emph{Hyper-Converged Storage}\index{Hyper-Converged Storage}
ist eine Komponente hyperkonvergenter Infrastruktur.
Dabei wird Virtualisierung genutzt,
um Speicherplatz zur Verfügung zu stellen
und monolithische Ansätze wie Storage Area Network und Network Attached Storage
zu ersetzen.
Der Storage ist virtualisiert und software-defined
und läuft neben anderen Funktionen wie Compute auf der selben integrierten Lösung.
Speichergeräte sind dabei an Einzelsystemen angeschlossen,
virtualisiert zu einem Speicherpool zusammengefasst und
stehen anschließend anderen Komponenten oder virtuellen Systemen zur Verfügung.
Dies soll Administration vereinfachen und Komplexität verringern und
zugleich eine hohe Flexibilität in der Zuweisung von Speicherplatz bringen.

Im Gegensatz zum Converged Storage,
der die Komponenten in einer vordefinierten Appliance vereint,
aber dennoch voneinander unabhängige Komponenten betreibt,
kombiniert Hyper-Converged Storage die Einzelsysteme
zu einem integrierten und virtualisierten Gesamtsystem.

Hyper-Converged Storage soll u.\,A. die Infrastruktur einfach und kostengünstig machen,
Komplexität verringern, IT-Infrastruktur konsolidieren und hohe Flexibilität bringen.
Dem gegenüber stehen jedoch größere Abhängigkeiten von einzelnen Anbietern und Lösungen,
da solche zusammenhängenden Systeme natürlich weniger modular aufgebaut sind.
Außerdem sind nicht alle Systeme und Funktionen mit HCS abbildbar.
Die Zuordnung zwischen Storage und physischen Systemen
ist in HCS aufgrund der Virtualisierung nicht mehr möglich.
Ein großes Problem hierbei ist,
dass eine getrennte Skalierung nicht wirklich möglich ist.
Bei HCS muss man immer Rechenleistung als auch Speicherkapazität zusammen skalieren.
Außerdem ist HCS langsamer als dedizierte Hardware-SANs.

\section[Cloud-Native]{Cloud-Native \cite{noauthor_was_nodate}}

\emph{Cloud-Native}\index{Cloud-Native} ist eine Vorgehensweise bei der Softwareentwicklung,
die den Betrieb der Anwendung in einer Cloud-Umgebung zum Ziel hat.
Statt auf im klassischen Ansatz üblichen monolithische Anwendungen,
wird hier verstärkt auf sog. \emph{Microservices}\index{Microservice} gesetzt,
die alle je einen Teil der Funktionalität bereitstellen und
erst durch Zusammenschaltung über APIs zu einer gesamten Anwendung werden.
Durch die starke Modularisierung der Anwendung in Microservices
wird sich eine einfache und hohe Skalierbarkeit erhofft,
die unter minimalen Hardwareaufwand maximale Leistung und hohe Verfügbarkeit erzielen
und automatische Skalierung abhängig von der Last ermöglichen soll,
während Kosten durch den Aufbau physischer Infrastruktur
und Ausfallzeiten während Softwareupdates vermieden werden sollen.

In der Entwicklung von Cloud-Native Anwendungen kommen agile Entwicklungsprozesse
zum schnellen Ausrollen von neuen Features und
containerbasierte Umgebungen für die Isolation der Microservices voneinander zum Einsatz.
Durch den Betrieb in der Cloud erfordern Cloud-Native Anwendungen grundsätzlich
eine funktionierende Internetverbindung
und bringen immer die damit einhergehenden Gefahren mit sich.
Die Angriffsoberfläche einer Cloud-Native Anwendung kann sich durch die Microservices
je nach Netzaufbau um die APIs zwischen den Microservices vergrößern
(insbesondere, wenn Microservices bei unterschiedlichen Cloud-Anbietern laufen).
Außerdem erhöht das Hintereinanderschalten von Microservices insbesondere die Tail Latency
und damit auch die gefühlte Geschwindigkeit der Anwendung.

\section[CNF]{CNF \cite{noauthor_cloud-native_nodate,redhat_vnf_nodate}}

CNF steht \emph{Cloud-Native Network Function}\index{Cloud-Native Network Function}\index{CNF}
und beschreibt NFs, die nach dem Cloud-Native-Prinzip entwickelt wurden.
Dazu werden NFs statt in VMs in Containern implementiert
und die Architektur zu Microservices umgebaut.
Dadurch wird sich versprochen,
die Anzahl und Verteilung von VNF-Instanzen in der NFVI (besser) steuern
und damit auch die VNFs (automatisch) horizontal skalieren zu können.
Durch Service-Discovery soll das Gesamtsystem robuster gegen Ausfälle einzelner Knoten werden.
CNF erlaubt es, mehr Dienste auf einem Host zu betreiben.
Dies kommt jedoch zu Lasten der Latenz,
die durch den Betrieb vieler Anwendungen auf einem System schlechter wird.

\section{Network Service Mesh}

Das \emph{Network Service Mesh}\index{Network Service Mesh} erlaubt es CNFs,
dem darunterliegenden Netzwerk zu beschreiben,
mit welchen Diensten die CNF reden können muss.
Das Network Service Mesh kümmert sich dann darum,
die Verbindungen (z.\,B. VPN-Tunnel) aufzubauen,
damit die Kommunikation zwischen den CNFs möglich wird.
Dies ist ähnlich zum VIM aus dem NFV-Umfeld.

\section{Sub-RTT Congestion Control}

Traditionelle Congestion Control Algorithmen nutzen Informationen,
die erst nach einer RTT zur Verfügung stehen,
um Congestion im Netzwek zu erkennen.
Beispiele hierfür sind Paketverlust, Dup-ACKs oder auch von ECNs (\autoref{sec:ecn}).
Ebenso erkennt Congestion Control auch die Verfügbarkeit von zusätzlicher Kapazität
im Netzwerk erst durch ACKs von der Gegenseite,
also erst nach mindestens einer RTT.

Diese Art der Congestion Control hat insbesondere bei Pfaden mit hoher Latenz das Problem, 
dass der Sender die Information über Congestion erst lange nach dem Auftreten erhält
und so möglicherweise weitere Segmente sendet,
was entweder die Congestion verstärkt oder schlimmstenfalls
weiteren Paketverlust und damit zusätzlichen Aufwand
bei der Recovery des Congestion Windows und bei der Retransmisson erzeugt.
Das schadet dem Durchsatz und belastet den Link zusätzlich.

Sub-RTT Congestion Control ist der Versuch, Informationen über (anstehende) Congestion,
die bei einem Segment mit Sequenznummer \(n\) auftritt,
bereits vor Empfang des dazugehörigen ACKs mit der ACK-Nummer \(n\) zu erkennen.
Das soll zu schnellerer Reaktion auf Congestion und damit
weniger Überlast auf dem Link sowie mehr Gesamtdurchsatz führen.

Verschiedene solcher Verfahren werden in der aktuellen Forschung untersucht.
So versucht bspw. FNCC \cite{xu_fncc_2024},
sog. „In-Network Telemetry“ (INT) von Switchen in ACK-Pakete einzubetten,
sodass der Sender noch vor Rückkehr eines ACK-Paketes erkennen kann,
wenn einer der Switche in einer (anstehenden) Congestion-Situation ist
und seine Senderate entsprend anpassen kann.
Bolt \cite{briscoe_low_2023} sendet Congestion Notifications von Switchen
direkt an den Sender des Flows
und aktualisiert das Congestion Window nach jedem Feedback,
um Latenzprobleme von ECN zu vermeiden und schnelle Reaktionen zu erzeielen.
SFC \cite{le_sfc_2023} wiederum leitet Pause-Frames an den Sender weiter,
um nicht zur den nächstgelegenen Hop vom Bottleneck über Congestion zu informieren,
sondern den Sender des TCP Flows.

Ein Kernproblem mit all diesen Verfahren ist die Erfordernis,
dass die Hops zwischen Sender und Empfänger das entsprechende Verfahren unterstützen müssen.
Das erschwert eine Implementierung in der Praxis,
da insbesondere Abwärtskompatibilität zu alter Hardware gegeben sein muss.
Ausnahme hierbei ist lediglich das Data Center Umfeld,
in dem es leichter möglich ist,
alle Switche mit entsprechender Unterstützung auszustatten,
in dem es aber auch deutlich geringere RTTs als in WAN-Umgebungen
und damit auch mit RTT-basierter Congestion Control recht schnelle Reaktionen
auf Congestion geben kann.

\section{Named Data Networking}

\section{LEDBAT}

\emph{Low Extra Delay Background Transport}\index{Low Extra Delay Background Transport}
(LEDBAT)\index{LEDBAT}
ist ein Congestion Control Algorithmus für datenintensive Hintergrundanwendungen,
bspw. Downloads von Software-Updates.
Ziel von LEDBAT ist es, andere (wichtige) TCP Flows nicht zu stören
und möglichst ohne Paketverlust und mit geringem Queueing-Delay
viele Daten zu übertragen.

LEDBAT arbeitet dabei hauptsächlich Delay-basiert.
So wird einerseits der (fixe) Grund-Delay ermittelt,
der sich aus Verarbeitung, Übertragung, Serialisierung, u.\,Ä. zusammensetzt
und dazu die Abweichung des Ende-zu-Ende-Delays berechnet,
der sich aus Grund-Delay und Queueing-Delay zusammensetzt.
Dies gibt eine Abschätzung für den Queueing-Delay und damit die geschätzte Congestion im Netzwerk.

Um das Congestion Window bei geringer Netzwerklast zu vergrößern,
wird die Differenz aus einem Ziel-Delay und dem (geschätzten) Queueing-Delay berechnet.
Diese Differenz fließt neben u.\,A. einer Gain-Konstante
in die Berechnung der cwnd-Vergrößerung ein,
sodass bei Nähe zum Zielwert das cwnd nur marginal vergrößert wird,
während bei Überschreitung des Ziels das cwnd verkleinert wird (wie bei einem P-Regler).
So wird versucht, dass ein LEDBAT Flow das Netz gar nicht erst so stark belastet,
dass Congestion auftreten kann.
Im Fall von Congestion,
die klassisch über Packet Loss oder ECN (\autoref{sec:ecn}) erkannt wird,
wird dennoch wie bei herkömmlichen Loss-basierten Congestion Control Algorithmen
das cwnd (höchstens einmal pro RTT) halbiert.

