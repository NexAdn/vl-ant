\chapter{Interfacing NICs}

Dieses Kapitel beschäftigt sich mit der Funktionsweise von Netzwerkkarten.

\section{Ingress and Egress Packet Handling in Servers}

Eingehende Pakete kommen am Port an, dekodiert das Paket und prüft die MAC-Adresse,
ob sie mit der MAC-Adresse der Karte übereinstimmt.
Anschließend wird die CRC-Checksumme geprüft.
Anschließend wird das Paket über den PCI-Bus in einen Ringpuffer im RAM geschrieben.
Wurde das Paket fertig geschrieben, wird ein Interrupt ausgelöst,
um die CPU über das neue Paket zu informieren.

Der Scheduler merkt sich den Eingang des Interrupts für spätere Behandlung
(\emph{Soft Interrupt}\index{Soft Interrupt}),
da sonst bei zu vielen Paketen die CPU „lahmgelegt“ werden kann.
Zu einem späteren Zeitpunkt behandelt der Treiber den Soft Interrupt,
indem er die Pakete von der Netzwerkkarte ausliest und an
den Network Layer des Kernels weitergibt.
Anschließend wird das Paket an einen Socket übergeben,
der die dazugehörige Anwendung über den Eingang des Paketes informiert
und die Daten des Pakets bereitstellt.

Der umgekehrte Weg läuft genau entgegengesetzt.
Die vom Socket entgegengenommenen Daten werden in IP- und Ethernet-Header verpackt,
an den Treiber übergeben und in einen Ringpuffer geschrieben,
der von der Netzwerkkarte für den Versand ausgelesen wird.
Anschließend informiert die Karte das Betriebssystem wieder per Interrupt
über den erfolgreichen Versand.

Diese Methode hat mehrere Performance-Probleme:
\begin{itemize}
	\item Speicherzugriffe
		(und damit verbundene Cache-Misses, Locking, Memory A\-lign\-ment, etc.)
	\item Hoher Speicherverbrauch für Puffer
		(und dadurch auch schlechtere Cache-Effizienz)
	\item Kopieren von Daten
	\item Interrupts
	\item Kontextwechsel
\end{itemize}
Solche Operationen sind sehr teuer und führen daher zu Performanceproblemen.
Wenn Dienste virtualisiert sind, finden zusätzlich noch Kontextwechsel
zum Hypervisor statt und möglicherweise kann es auch durch Sicherheitsmechanismen
zu weitere Kopieroperationen kommen.

\section{Modern OS and NIC Features}
\subsection{Scatter Gather Listen}

Experimente, Kopien an der CPU vorbei (z.\,B.~direkt per DMA) durchzuführen,
hat sich als fehlerhaft herausgestellt.
Das Ziel ist also, möglichst wenige Kopien zu erstellen,
idealerweise ein Paket nie zu kopieren.
Stattdessen soll ein Zero-Copy-Ansatz verfolgt werden,
bei dem im Betriebssystem nur Pointer auf Pakete statt den vollständigen Paketen
kopiert werden.

Alle modernen Netzwerkkarten unterstützen dafür
\cdefname{Scatter Gather Listen}\index{Scatter Gather Liste},
die Pakete nicht mehr als zusammenhängendes Segment speichern,
sondern in Segmenten, über die mit einem Paketdeskriptor zugegriffen wird.

Dieser Mechanismus erfordert dennoch große Segmente,
da jeder Zugriff auf ein Segment zu zusätzlichem I/O führt.
Außerdem müssen die Anwendungen Scatter Gather Listen unterstützen
und statt den üblichen \texttt{recv} und \texttt{send} Calls die
komplizierteren \texttt{recvmsg} und \texttt{sendmsg} Calls benutzen.

\subsection{Head-, Tailroom and Header Split}

Die Idee ist, bei der Speicherallokation vor und nach den Nutzdaten direkt
Speicher zu allokieren, der von der Netzwerkkarte beschrieben werden kann,
um Header und Trailer (z.\,B.~für IPsec) zu schreiben.
Solange der Head- und Tailroom ausreicht, können so Kopien vermieden werden.
Allerdings ist nicht unbedingt bekannt, wie viel Headroom benötigt wird.
Außerdem besteht so das Problem,
dass die Daten nicht unbedingt aligned sind, was der Cache-Effizienz schadet.
Dieses Problem lässt sich nicht wirklich lösen,
da nicht bekannt ist, wie groß der Headroom sein muss.
Weitere Probleme treten auf,
wenn Header (z.\,B. VLAN Tags, MPLS Label, IPSec)
inmitten des Paketes eingefügt werden müssen
oder auch wenn ein Paket an mehrere Empfänger versendet werden soll.

Um das Problem (theoretisch) zu lösen, können Netzwerkkarten Pakete tiefer analysieren
und die Headerdaten getrennt von den Nutzdaten in verschiedenen Segmenten
einer Scatter Gather Liste abzulegen.
So können die Daten immer aligned werden
(da das Segment an einer Cache-Grenze allokiert wird)
und der Versand an mehrere Empfänger ist auch effizient möglich,
da die Headersegmente alle auf die gleichen Nutzdaten zeigen können.
In der Praxis hat sich dies jedoch nicht bewährt,
da Spezialfälle wie Fragmentierung, etc. von der Netzwerkkarte nicht behandelt werden.
Daher muss auch noch der konventionelle Datenpfad durchlaufen werden.
Durch den Split sind außerdem immer mehrere DMA-Transfers notwendig,
um auf das gesamte Paket zuzugreifen.
Daher wird es in der Praxis auch nicht wirklich benutzt.

\subsection{IP and TCP Offloading}

Die Idee ist, Aufgaben von der CPU auf die Netzwerkkarte auszulagern.
Dafür müssen Netzwerkkarten die Pakete besser verstehen.
Dafür müssen Netzwerkkarten entsprechende Offloading-Features unterstützen,
um bspw.~Checksummen von IP- oder TCP-Headern zu berechnen,
sodass die CPU an der Stelle einfach einen Dummy-Wert eintragen kann.

\cdefname{Large Receive Offload} und \cdefname{Large Segment Offload}
\index{Large Receive Offload}\index{Large Segment Offload}
erlauben es Netzwerkkarten, TCP auf der Netzwerkkarte zu interpretieren.
So kann die Netzwerkkarte eingehende Bursts eines TCP Streams gemeinsam behandeln,
um diese bspw.~zu größeren Paketen zusammenzufügen,
bevor die Pakete an das Betriebssystem weitergegeben werden.
So kann die CPU mit weniger Speicherzugriffen größere Mengen an Daten von der Netzwerkkarte empfangen.
Umgekehrt kann Large Segment Offload große Pakete vom Betriebssystem
auf der Netzwerkkarte in kleinere TCP-Pakete aufteilen.
Insgesamt führt dies dazu, dass es zu weniger Interrupts und weniger Speicherzugriffe führt
und weniger Routing Overhead auftritt.

Probleme mit dieser Aggregierung treten jedoch dann auf,
wenn Pakete klein zwecks Latenz klein bleiben sollen
(z.\,B.~bei interaktiven Sitzungen, VoIP, etc.).
Solche Anwendungen können also nicht optimiert werden und müssen daher
wieder über den langsame(re)n Pfad bearbeitet werden.
Damit werden also eher die großen Flows beschleunigt,
die gar nicht so sehr darauf angewiesen sind, möglichst schnell verarbeitet zu werden.

\subsection{Multi-Queue NICs}

Der Zugriff auf Queues wird normalerweise über Locking geschützt,
wenn mehrere CPUs die Queues auslesen sollen.
Das macht Zugriffe sehr langsam.
Moderne Netzwerkkarten unterstützen dagegen mehrere Queues pro Richtung,
sodass jede CPU ihre eigene Queue erhält, über die Pakete empfangen werden können,
ohne vorher ein Lock auf die Queue setzen zu müssen.
Durch mehrere Queues können bspw. auch virtuelle Maschinen eigene Queues erhalten,
um unabhängig voneinander ohne Locking Pakete empfangen zu können.

Beim Empfangen muss jetzt jedoch genauer darauf geachtet werden,
welche Pakete in welche Queue abgelegt werden.
Einfache Round-Robin-Verteilung von Paketen (und andere einfache Methoden) 
führen dazu, dass Pakete für die gleiche Anwendung auf mehreren CPUs ankommen
(wodurch für die Zusammenführung wieder Locking nötig ist) bzw.
Reordering auftreten kann.

Um bessere Effizienz zu erzielen, gibt es Mechanismen wie
\cdefname{Flow Director}\index{Flow Director},
die beim Versand von Paketen eines Flows den umgekehrten Flow (Sender/Empfänger vertauscht)
auf die RX-Queue der entsprechenden CPU per Flow-Regel zu mappen.
Dies führt zu guter Cache-Nutzung und damit guter Performance auch auf Anwendungsseite,
hat aber hohe Anforderungen an die Netzwerkkarte.
Außerdem ist dies durch die Anzahl Flow-Regeln limitiert,
die auf der Netzwerkkarte hinterlegt werden können.

Dazu gibt es eine etwas zustandslosere Alternative,
die rein auf den Paketheadern der eingehenden Pakete arbeitet,
ohne Daten speichern zu müssen.
Bei \cdefname{Receive Side Scaling}\index{Receive Side Scaling}
wird ein sog. \emph{Toeplitz-Hash}\index{Toeplitz-Hash} über Headerfelder berechnet,
der sich innerhalb von Paketen eines Flows nicht ändert.
Der Hashwert wird dann benutzt, um den Index einer Receive Queue zu erhalten.
Bei der Berechnung des Toeplitz-Hashes wird ein Key eingerechnet,
der geheimzuhalten ist,
da ansonsten unter Kenntnis des Schlüssels
ein DoS-Angriff auf eine Anwendung potentiell dadurch geführt werden kann,
dass nur Flows erstellt werden, die eine einzelne CPU lahmlegen.
Außerdem wird auch das Problem nicht gelöst,
dass \emph{Elephant Flows}\index{Elephant Flow} weiterhin
nur von einer CPU bearbeitet werden können.

\subsection{Increasing Speed for Virtual Machines}

Das Ziel ist jetzt, auch für jede VM eine eigene Netzwerkkarte zu geben.
Damit soll vermieden werden, dass Hypervisor Pakete behandeln müssen.
Statt jetzt jedoch mehrere Netzwerkkarten im VM-Host zu verbauen
(was schlecht skalieren würde und sehr aufwändig ist),
wird \cdefname{SR-IOV}\index{SR-IOV} (Single-Root Input/Output Virtualization)
eingesetzt.
Dabei meldet sich die Netzwerkkarte am System als mehrere virtuelle Netzwerkkarten
(PCI IDs),
geteilt in \emph{Physical Functions}\index{Physical Function} (PF),
die das „echte“ Gerät mit allen Funktionen widerspiegeln und
auch zur Administration der Netzwerkkarte genutzt werden,
und den \emph{Virtual Functions}\index{Virtual Function} (VF),
die stark eingeschränkte virtuelle NICs sind,
welche von Gast-Systemen benutzt werden können.
Der Hypervisor kann dann eine solche VF als Netzwerkkarte direkt einer VM zuweisen,
sodass Interupts, Pakete, etc. direkt an die VM statt an den Hypervisor geleitet werden.
Andere Funktionen können je nach Hersteller auch bereitgestellt werden,
so bspw. Möglichkeiten, den Traffic von Gastsystemen zu steuern oder auch
Kommunikation zwischen VFs mit einem virtuellen Switch im NIC zu ermöglichen,
statt auf Hairpinning\footnote{Siehe \autoref{sec:hairpin}} setzen zu müssen.

Auf der Host-Seite werden jetzt für jede VM eigene Ringpuffer benötigt
und die IOMMU muss so konfiguriert werden,
dass jede VM nur auf die ihr zugewiesene virtuelle Netzwerkkarte zugreifen kann.
Um Pakete an die passende virtuelle Netzwerkkarte zu leiten,
ist auf der physischen Netzwerkkarte ein virtueller Switch,
der bspw. anhand der virtuellen MAC-Adressen erkennt,
welches Paket an welche virtuelle Netzwerkkarte geschickt werden muss.
Dieser virtuelle Switch muss nicht nur eingehende Pakete an die richtige VF leiten,
sondern auch zwischen VFs (Ringen der VMs) Pakete weiterleiten können.

\subsection{Currently Trending Optimizations}

Verschiedene Ansätze sind aktuell im Trend bei Networking.
Moderne Netzwerkkarten erhalten zunehmen mehr Funktionen,
so z.\,B. RDMA over Converged Ethernet (\emph{RoCE}\index{RoCE}).
Weiter werden diese auch mit normalen General-Purpose Koprozessoren ausgestattet,
so z.\,B. mit GPUs, FPGAs oder auch vollwertigen CPUs,
die eigene Betriebssysteme laufen lassen können
(sog. \emph{Data Processing Units}\index{Data Processing Unit}, \emph{DPUs}\index{DPU}).

Weitere Verbesserungen liegen im Bereich der Speicherzugriffsgeschwindigkeiten.
Einerseits werden CPU Caches vergrößert,
um auf mehr Daten schnell zugreifen zu können.
Um die von der Netzwerkkarte mit DMA geschriebenen Daten schnell auslesen zu können,
werden zudem die dazugehörigen DMA-Regionen teilweise direkt in den L3-Cache platziert,
sodass beim Auslesen des Paketes der zusätzliche Schritt,
das Paket aus dem RAM zu laden, entfällt.
Weiter wird zur Speicherung großer Speicherbereiche auf vergrößerte Speicherseiten
(\emph{Huge Pages}\index{Huge Page}) gesetzt,
die den Zugriff auf größere hintereinanderliegende Speicherbereiche ermöglichen,
ohne viele Page Faults auszulösen.

Die Entwicklung in der Softwarearchitektur geht dahin,
dass Locking-Mechanismen weitgehend vermieden werden.
Ein Beispiel hierfür ist der Linux Express Data Path (XDP) \cite{noauthor_express_2024}.

\section{Software Architectures for Efficient Network Appliances}

\subsection{Reducing Context Switches}

Durch die geschichtete Softwarearchitektur moderner Betriebssysteme
gibt es viele Kontextwechsel,
die natürlich langsam sind.
Das lässt sich in NICs nicht direkt umgehen.
Stattdessen ist die Idee,
Performance-kritische Anwendungen direkt im Kernel auszuführen,
um Kontextwechsel zu sparen.
Beispiele dafür sind NFS Server, Web Server (z.\,B.~TUX), TLS.
Das erlaubt nicht nur das Ersparen von Kontextwechseln,
sondern ermöglicht auch eine bessere Kontrolle darüber,
welche Features bspw. auf eine Netzwerkkarte ausgelagert werden können (z.\,B.~TLS).

Hauptproblem ist hier die Sicherheit.
Wenn Bugs auftreten, kann dies das komplette System betreffen.
Fehler lassen sich im Kernel nur schwer debuggen,
was auch die Entwicklung schwierig macht.
Außerdem sind die Schnittstellen (und damit Features) im Kernel stark eingeschränkt,
was die Nützlichkeit einschränkt.

Ein besserer Ansatz ist, nur einzelne Features zu outsourcen,
bspw.~Übertragung einzelner Dateien auf einem Socket,
statt einer kompletten HTTP-Implementierung.
So kann man viele Kontextwechsel, die für die Übertragung großer Daten nötig wären,
ersparen.
Aber auch hier eignen sich die Features nur für wenige spezielle Anwendungen.
Auch dies ist also keine generelle Lösung.

Eine andere Idee ist,
den Kernel als Zwischenschritt beim Networking zu eliminieren.
Statt Code zur Paketverarbeitung im Kernel auszuführen,
wird sämtlicher Code im Userspace ausgeführt.
Dafür muss der Speicher der Netzwerkkarte direkt
in den Userspace einer Anwendung gemappt werden.
Das hat jedoch den Nachteil,
dass immer nur eine Anwendung auf ein NIC zugreifen kann.\footnote{
	Mit manchen Netzwerkkarten ist es auch möglich,
	über Flow-Regeln Queues auf Anwendungen aufzuteilen,
	aber das ist schwierig umzusetzen.
}
Dafür werden aber sämtliche Kontextwechsel für die Paketverarbeitung vermieden.
Außerdem kann die Anwendung statt Interrupts Polling benutzen,
was Verzögerungen und Unregelmäßigkeiten durch Interrupts vermeidet
und bei guter Programmierung auch geringe Latenzen (<\(100\mu s\)) erzielt.
Das wiederum kann die Performance bspw. für Netzwerkspeicher deutlich verbessern.
Hauptnachteil bei diesem Ansatz ist,
dass die Anwendung den kompletten Netzwerkstack (von ARP bis TCP) selbst implementieren muss.
Dies wird teilweise aber von Frameworks übernommen.
Bibliotheken hierfür sind beispielsweise das Intel Data Plane Development Kit (DPDK),
Solarflare OpenOnload oder auch PF\_RING.

\subsection{Multicore Scheduling}

Um hohe Paketraten verarbeiten zu können,
müssen Pakete auf mehreren CPUs parallel verarbeitet werden können.
Dafür können verschiedene Modelle genutzt werden.

Im \cdefname{Push to pull path}\index{push to pull} werden Pakete
von einem Thread nach dem Empfang bis zu einer Transmit Queue verarbeitet.
Ein anderer Thread nimmt die Pakete aus der Transmit Queue
und verschickt sie auf dem NIC.
Beim \cdefname{Full push path}\index{full push}
werden Pakete von dem Thread verschickt, der auch die Pakete empfangen hat.

Das Push to pull Design hat das Problem,
dass Daten zwischen Threads ausgetauscht werden müssen,
was sehr cache-ineffizient und damit langsam ist.
Außerdem muss zwischen den Threads kommuniziert werden,
welche Speicherbereiche wieder vom Input Thread benutzt werden können.
Außerdem laufen die Input Threads unterschiedlich schnell zu den Output Threads,
weshalb ein Scheduler eigentlich den jeweils schnelleren Thread weniger häufig ausführen müsste,
damit der langsamere Threads mehr Pakete verarbeiten kann.
Das Full push path Design vermeidet Kommunikation zwischen Threads weitestgehend,
was sich positiv auf die Geschwindigkeit auswirkt.
Da der Input Thread auch das Paket verschickt, ist ihm auch bekannt,
welche Speicherbereiche wieder für den Empfang von Pakete benutzt werden können.
Außerdem muss ein Scheduler
(unter der Annahme, dass alle Threads ähnlich viel Input-Last haben)
nur dafür sorgen, dass Threads ähnlich häufig ausgeführt werden.

Ein weiteres Problem ist die Frage, wie Daten zwischen Threads geteilt werden.
Hier gibt es auch wieder die Möglichkeit,
sowohl Daten zwischen Threads zu teilen (mit threadsicheren Datenstrukturen)
als auch jedem Thread nur Daten zu geben,
die er tatsächlich benötigt.
