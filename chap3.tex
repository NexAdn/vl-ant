\chapter{Size and Organization of Router Buffers}
\label{sec:buffers}

\section{Routers as Queue Servers}

In diesem Kapitel nehmen wir eine Sender an, der TCP über einen Link sendet.
Die Pakete kommen per Gigabit Ethernet bei einer FritzBox an,
die per DSL ins Internet mit O2-Geschwindigkeit senden muss.
Damit ist die FritzBox der einzige Bottleneck-Link (was auch üblich ist).
Während also der Ethernet-Link nicht ausgelastet ist,
ist auf dem DSL-Link Stau.
Die Kernfrage ist jetzt, wie groß man die Queue machen muss.
Modelliert wird der Router als M/M/1/B-System (1 Router mit Puffergröße \(B\)).

Nach der Formel für die Loss-Wahrscheinlichkeit konvergiert der Paketverlust
durch steigendes \(B\).
In der Theorie kann also durch einen Puffer die Loss-Wahrscheinlichkeit gering gehalten wreden.
Allerdings ist das Modell nicht realistisch,
da TCP Bursts generiert (also der Ankunftsprozess kein Poisson-Prozess ist).
Grundsätzlich gilt aber:
\begin{itemize}
	\item Je größer der Puffer, umso teurer (allerdings durch DRAM nicht sehr teuer)
	\item Je größer der Puffer, umso größer die Latenz
	\item Je größer der Pfufer, umso geringer die Loss-Wahrscheinlichkeit
\end{itemize}

Ziel ist jetzt, den Bottleneck-Link möglichst durchgängig auszulasten.
Das ist ein Argument für große Puffer, da so Schwankungen im Ingess Traffic
ausgeglichen werden können.
Dagegen spricht jedoch, dass TCP ohne ECN\footnote{\autoref{sec:ecn}}
den Puffer zunächst immer komplett füllt,
bevor Congestion Control die Senderate reduziert.

\section{TCP ACK / self-clocking}

TCP sendet neue Daten immer,
wenn das Congestion Window größer ist als die Menge versendeter Daten,
die noch nicht geACKt wurden.
ACKs dienen also nicht nur der Bestätigung, dass Pakete angekommen sind,
sondern erlauben dem Sender auch, mehr Daten zu senden.

Die AIMD-Phase vom TCP führt dabei zu einem Sägezahn-Muster,
welches die Menge ungeACKter Daten im Netz (das Congestion Window) additiv erhöht
und multiplikativ verkleinert.
In der Praxis folgt TCP jedoch nicht ganz diesem Muster.
In der Initialisierungsphase wird Slow Start ausgeführt,
d.\,h.~das Congestion Window erhöht sich exponentiell,
bis der intiale Congestion Threshold erreicht wurde.
Dies ist das Verhalten des (alten) TCP Tahoe.
TCP Reno erweitert dies um Mechanismen für Fast Recovery nach Congestion,
hat aber immer noch Einbrüche im Congestion Window (und damit der Senderate).
Die FritzBox muss also für eine optimale Auslastung des Links solche Einbrüche
durch Puffer kompensieren können.

\section{Deepen understanding of TCP Reno}

Ziel ist, das Sägezahnmuster von TCP Reno genauer zu analysieren.
Dazu werden zunächst Differentialgleichungen für die Größe des Congestion Window
aufgestellt und aufgelöst:

\begin{align}
	W &= C \cdot RTT\\
	&= C \cdot \left(2 T_p + T_q\right)\\
	&= C \cdot \left(2 T_p + \frac{Q\left(t\right)}{C}\right)\\
	\dot{W} &= \frac{1}{RTT} = \frac{1}{2T_p + \frac{Q\left(t\right)}{C}}\\
	&= \frac{C}{W}\\
	W \frac{\dd W}{\dd t} &= C\\
	W \cdot \dd W &= C \cdot \dd t\\
	\cuint{W}{W} &= \cuint{C}{t}\\
	\frac{1}{2} W^2 &= Ct + k\\
	\shortintertext{Wir können annehmen, dass die Warteschlange zu Beginn leer (\(Q\left(0\right) = 0\)) ist:}
	W\left(0\right) &= C \cdot 2T_p\\
	\frac{1}{2} \cdot \left(2CT_p\right)^2 &= 0 + k\\
	2C^2 T_p^2 &= k\\
	\implies W &= \sqrt{
		2C + 4C^2 T_p^2
	}
\end{align}
Das Muster ist also gar kein Sägezahnmuster, sondern folgt einer konkaven Funktion!
Je mehr gesendet wird, umso mehr Queueing tritt auf.
Dadurch steigt das Delay, weshalb die Senderate sich zunehmend langsamer erhöht,
bis tatsächlich Congestion auftritt und das Congestion Window halbiert wird.

\section{The Single Flow Case}

Zunächst wird angenommen, dass in unserem Szenario nur eine TCP-Verbindung exisitert.
Diese Verbindung füllt also langsam die Puffer, bis dieser vollläuft.
Da jetzt Congestion auftritt, halbiert sich die Größe des Congestion Windows
von \(W^{\ast}\) auf \(\frac{W^{\ast}}{2}\).
Da aber noch ca. \(W^{\ast}\) unterwegs sind, muss TCP warten,
bis \(\frac{W^{\ast}}{2}\) Pakete geACKt wurden.
In dieser Zeit, in der nichts gesendet wird,
muss der Router aber die Queue leeren, um maximalen Durchsatz zu erzielen.
Nimmt man an, dass die Gegenstelle jedes eingehende Paket ACKt,
dauert es \(\frac{\frac{W^{\ast}}{2}}{C}\), bis der Sender wieder senden kann.
Der Puffer muss also groß genug sein, um mindestens so lange senden zu können,
also mindestens \(\frac{W^{\ast}}{2}\).
Da TCP Congestion Windows üblicherweise im Bereich mehrerer Megabyte sind,
müssen die Puffer also sehr groß sein!

Betrachtet man nun die Situation nach der Wartezeit,
dann ist der Puffer leer.
Das aktuelle Congestion Window ist \(W = \frac{W^{\ast}}{2}\).
Um den Bottleneck Link voll auszulasten,
muss der Sender idealerweise mit Rate \(C\) senden.
Da \(R = \frac{W}{RTT}\), muss \(R = C = \frac{W}{RTT}\) gelten.
Also \(C = \frac{\frac{W^{\ast}}{2}}{2T_p}\).
Durch Einsetzen und Auflösen gilt \(B \geq 2T_p C\).
Je größer also die Linkkapazität (oder die RTT), umso größer müssen also die Puffer sein.
Während also in Data Centern kleine Puffer aufgrund der kleinen RTT ausreichen,
kann bei großen Latenzen die benötigte Puffergröße extrem ansteigen.

Leider kann TCP auch nicht auf unterschiedlich große Puffergrößen reagieren,
da TCP nicht genau erkennen kann, ob der Bottleneck Link zu große oder zu kleine Puffer hat.

\section{Many Flows: Synchronized or Not?}

Bisher wurde nur das Verhalten einer einzelnen (isolierten) Verbindung diskutiert.
Die Frage ist jetzt, wie sich das Netz verhält, wenn es viele Flows gibt.
Die Frage ist zunächst, ob TCP sich synchronisieren kann,
d.\,h.~ob mehrere Verbindungen gleichzeitig das Congestion Window erhöhen.
In so einem Fall verhält sich aus Sicht des Routers TCP wie ein großer TCP Flow.
Das kann passieren, wenn wenige Flows ungefähr gleiche RTTs zu den Zielen haben,
da sie dann ähnliches Verhalten haben, nachdem sie durch Congestion in der Queue
alle einmal (etwa zeitgleich) erkannt haben, dass Congestion aufgetreten ist.

Bei vielen Flows wird das Verhalten chaotischer und Synchronisation tritt nicht mehr auf.
Dann ist das Sendeverhalten gleichmäßiger.
Dadurch ist der Unterschied zwischen maximaler und minimaler Puffergröße geringer,
weshalb die Puffer kleiner gewählt werden können.
Das Verhalten der TCP-Flows lässt sich auch mathematisch analysieren.
Dazu werden die Congestion Windows der Flows zu einem Window aggregiert.
Dieses bewegt sich ungefähr in der Größenordnung der Queue-Größe.
Die \(W_i\) werden bei der Analyse als normales Sägezahn-Muster
(ohne die konkave Form) angenommen.
Das Sägezahn-Muster schwankt zwischen \(\frac{2}{3}\) und \(\frac{4}{3}\) um einen Erwartungswert.
Unter der Annahme, dass es ein echter Sägezahn ist
(also mit linearem Anstieg abseits der Sprungstelle),
ist die Window-Größe dann uniform verteilt.
Für jeden Flow gibt es jetzt also die Window-Größe \(W_i\) als Zufallsvariablen,
welche sich aufaddieren und stochastisch analysieren lassen.

Es ergibt sich für die Standardabweichung eine obere Schranke.
Diese ist umgekehrt proportional zur Anzahl der Flows!
Je mehr Flows also auf dem Link sind, umso geringer die Standardabweichung.
Es ist also bei Core Routern mit vielen Flows nicht nötig, große Puffer zu erhalten,
um Schwankungen im Congestion Window auszugleichen.

\section{Short Flows}

Allerdings ist in modernen Szenarien nicht mehr wichtig,
dass große Verbindungen mit vielen Daten möglichst viel übertragen wird.
Die üblichen Szenarien moderner Webanwendungen (z.\,B.~HTTP, Instant Messaging)
werden so wenige Daten übertragen,
dass TCP die Slow Start Phase ne verlässt.
Für die Analyse wird jetzt eine unendliche Geschwindigkeit
und auch ein \textsc{Poisson}-Prozess angenommen.
Auch dies ist in modernen Netzwerken nicht der Fall,
erleichtert aber die Analyse.

Der Router wird hier zu einem \emph{M/G/1-System}\index{MG1-System@M/G/1-System}
(d.\,h.~Markov-Ankunftsprozess mit generisch verteiltem Verarbeitungsprozess).
Mit lustiger Mathe lässt sich der Erwartungswert für die Verarbeitungszeit \(T_v\)
berechnen.
Dabei zeigt sich, dass die Zeit für hohe Last (\(\rho\)) extrem ansteigt.
Die Verteilung der Burst-Größen ist allerdings sehr meh verteilt.
Da bei größeren Paketzahlen auch kleinere Bursts verschickt werden,
springen die Häufigkeiten der Burstgrößen an den Sprungstellen (Zweierpotenzen)
stark nach oben an,
während alle anderen Häufigkeiten deutlich kleiner sind
(weil sie nur für Rest-Bursts auftreten können).
Insgesamt klingen die Häufigkeiten Burst-Größen exponentiell ab.

Die Durchschnittliche Queue-Länge zeigt auch Spikes in der Nähe Grenzen der Burst-Größen.
Dazwischen gibt es jeweils ein lokales Minimum, was dadurch bedingt ist,
dass für Restpakete im letzten Burst weniger Paket verschickt werden.
Spikes ergeben sich an den Stellen, wo Bursts immer voll ausgenutzt werden.

Viele kleine Flows, die die Slow-Start-Phase nicht verlassen,
akkumulieren sich nicht.
Es ist für kleine Flows nicht notwendig, extrem große Puffer zu maintainen.
Das Buffer-Bloat-Problem ist also rein darauf basierend,
dass das Messszenario eine möglichst große Bandbreite bei großen Dateiübertragungen ist.
Es ist vielleicht also doch nicht so sinnvoll, die Buffer sehr groß zu halten.

\section{Very Small Buffers?}

Mit dem bisherigen Modell ist es problematisch,
Puffer richtig klein zu gestalten,
da TCP ansonsten viel Paketverlust und damit geringe Performance erzielt.
Um also geringe Puffer zu benutzen,
muss TCP dazu gebracht werden, die Senderate zu limitieren.

Durch Anwerfen der großen Markov-Mühle ergibt sich ein schöner Term,
bei dem mit \(\frac{1}{\rho}\) logarithmiert wird.
Also hält sich die benötigte Puffergröße hier im Rahmen.
Dabei ist die benötigte Puffergröße nicht mehr von der Anzahl Flows
oder der Linkkapazitäten abhängig.
Solange man also nicht mehr den vollen Durchsatz erzielen können will,
können auch kleine Puffer genutzt werden.
Allerdings wird dafür immer noch ein \emph{paced TCP}\index{TCP!paced} benötigt,
was keine Bursts, sondern bei einer fixen Rate sendet.

Die Benutzung kleiner Puffergrößen ist insofern wünschenswert,
als dass kleine Puffer auch gut in schneller Hardware (z.\,B.~SRAM)
statt langsamer Hardware (z.\,B.~DRAM) implementierbar sind.

\section{Active Queue Management}

Wenn Queues voll werden, erhöht sich die RTT.
Dadurch wird Congestion schwer vermeidbar.
Das liegt zum einen daran, dass Flows langsamer reagieren,
da Paketverluste am Ende der Queue auftreten
und es bei vollen Queues entsprechend lange dauert,
bis das verloren gegangene Paket die Queue „durchlaufen“ hat
und erkannt wurde\footnote{
	Gemeint ist hier natürlich nicht,
	dass ein verlorenes Paket tatsächlich durch die Queue läuft.
	Allerdings kann TCP das verloren gegangene Paket erst dadurch erkennen,
	dass bspw. mehrere Dup-ACKs durch das Eintreffen nachfolgender Segmente am Empfänger
	ausgelöst oder (deutlich größere) Timeouts abgelaufen sind.
	Beides dauert mindestens die Zeit,
	die die nachgelagerten Pakete benötigen, um die Queue zu durchlaufen.
	Entsprechend ist die Verzögerung etwa durch \(2T_p + T_q\) nach unten beschränkt.
}.
Andererseits können ACKs näher zusammenliegen als normal,
was die Burstiness des Flows und damit die Congestion verstärkt.
Je größer die Kapazität der Queue,
umso weiter ist das „Wissen“ der Endpunkte über Congestion von der Wahrheit entfernt,
da eigentlich schon beim Auffüllen der Queue Congestion auftritt,
dies aber von den Flows noch nicht erkannt wird.
Zudem ist es bei großen Queues wahrscheinlicher,
dass diese sich gar nicht mehr vollständig leeren
(da TCP schneller die Senderate hochfährt als die Queue geleert werden kann),
wodurch Buffer Bloat entsteht
und dadurch große Delays unter Last normal werden.

Eine gute Puffergröße ist nur schwer abzuschätzen,
da dies nicht zuletzt auch von \(T_p\) abhängt.
Dies ist ein Problem, da \(T_p\) nicht bekannt ist und von Umgebung zu Umgebung
schwankt (z.\,B.~extrem klein in Data Centern, aber extrem groß bei Satelliten).
Stattdessen soll jetzt versucht werden, die Queue-Größe dynamisch zu verändern,
indem noch vor dem tatsächlichen Volllaufen der Queues Pakete verworfen werden.
Dies erlaubt es auch TCP, schneller Congestion zu erkennen und
das Congestion Window zu verkleinern.
Verfahren dieser Art werden unter
\emph{Active Queue Management}\index{Active Queue Management}
zusammengefasst.

\subsection{Random Early Drop}

Die Idee von \cdefname{Random Early Drop}\index{Random Early Drop} ist,
ab einer gewissen Queue-Auslastung probabilistisch Pakete zu verwerfen,
obwohl noch Platz in der Queue wäre.
Die Wahrscheinlichkeit dafür wird abhängig von der Queue-Größe
und zwei Thresholds (\(min_{th}\), \(max_{th}\)) gewählt.

ECN schätzt die Queue-Größe als exponentielle gewichteten gleitenden Durchschnitt
mit Gewicht \(w_q\) ab.
Dieser wird bei jedem Eintreffen eines Paketes berechnet:
\begin{align*}
	avgq_{now} &= w_q \cdot qlen_{now} + (1 - w_q) \cdot avgq_{prev}\\
	\shortintertext{
		Für den Fall, dass die Queue leer ist (\(qlen_{now} = 0\)),
		wird damit approximiert,
		dass \(m\) kleine Pakete hätten verarbeitet werden können,
		um \(avgq_{now}\) abklingen zu lassen:
	}
	m &= \frac{t_{now} - t_{last\_arrival}}{pkt\_size_{nominal}}\\
	avgq_{now} &= \left(1 - w_q\right)^m \cdot avgq_{prev}\\
	\shortintertext{
		Die Drop-Wahrscheinlichkeit \(P_M\)
		wird nun durch lineare Interpolation zwischen \(min_{th}\) und \(max_{th}\)
		mit einer Maximalwahrscheinlichkeit \(P_{max}\) berechnet:
	}
	P_M\left(avgq_{now}\right) &= P_{max} \cdot \frac{avgq_{now} - min_{th}}{max_{th} - min_{th}}
\end{align*}
Statt Pakete zu droppen,
können aber auch Pakete mit ECN\footnote{siehe \autoref{sec:ecn}} geflaggt werden,
um TCP zu signalisieren, dass Congestion auftritt,
ohne direkt Nutzdaten zu verwerfen.

Durch die Glättung (\(avgq\)) werden Schwankungen der Queue-Größe durch Bursts ausgeglichen.
Mit \(w_q\) lässt sich die Verzögerung und Window-Größs der Durchschnittsberechnung
einstellen.
Ein kurzes Window (großes \(w_q\)) reagiert schnell,
ist aber auch Anfällig für Transienten,
während ein langes Window (kleines \(w_q\)) zwar resistenter gegenüber Transienten ist,
sich dafür aber an plötzliche Veränderungen der Queue-Größe nur langsam anpasst.\footnote{
	Der EWMA kann als eine Art Tiefpassfilter interpretiert werden,
	d.\,h. niederfrequente (langsame und dauerhafte) Änderungen
	spiegeln sich am Ausgang (im Rechenergebnis) wider,
	während hochfrequente (schnelle und transiente) Änderungen
	kaum den Ausgang (das Rechenergebnis) im Vergleich zum vorigen Wert ändern.
}
Durch \(min_{th}\) kann die „Power“ des Netzwerks angepasst werden.
Kleine \(min_{th}\) (nahe 0) führen dazu,
dass Pakete früh gedroppt werden und die Wahrscheinlichkeit
von Idle-Perioden (Queue ist leer, Bandbreite wird nicht voll ausgenutzt) steigt.
Ist \(min_{th}\) zu weit von 0 entfernt,
wird die Pfad-Latenz erhöht und damit das Feedback-Signal
an die Endpunkte nur verzögert weitergegeben,
wodurch auf Congestion langsamer reagiert wird.
\(max_{th} - min_{th}\) hingegen beeinflusst die Häufigkeit von Markierungen/Drops.
Ein zu kleiner Abstand führt dazu,
dass das AQM zu ungleichmäßig reagiert
(d.\,h. dass Flows scheinbar chaotisch abwechselnd sehr viele und sehr wenige Drops erfahren)
und Flows zur Synchronisation zwingt.
Der Abstand muss größer als die typische durchschnittliche Vergrößerung von \(avgq\)
innerhalb einer RTT sein.

Diese Parameter richtig einzustellen besteht immer aus Kompromissen,
die von Netz zu Netz konfiguriert werden müssten.
Zudem hängen diese auch teilweise von der RTT der Flows ab.
Das macht die Einstellung der Parameter schwierig und
damit RED in der Praxis nur schwer nutzbar.

\subsection{Fair RED / Flow RED (FRED)}

Die Idee von FRED ist,
abhängig vom Flow-Typ unterschiedlich zu reagieren
(bspw. nicht-adaptive Flows auch nicht beeinflussen).
Dazu wird Accounting pro aktivem Flow in der Queue geführt
und Packet Loss abhängig von der Anzahl Pakete in der Queue pro Flow reguliert.
Alle Flows dürfen bis zu \(min_q\) Pakete in der Queue haben,
ohne Packet Loss zu erfahren.
So können kleine Flows (mit geringen Senderaten, bspw. IM, VoIP) ungehindert
durch die Queue laufen, ohne durch Packet Loss gestört zu werden.
\(min_q\) wird dabei anhand der durchschnittlichen
pro-Flow-Belegung der Queue (\(avgcq\)) bestimmt.
Es wird eine obere Grenze \(max_q\) pro Flow-Typ gesetzt
und die Verletzungen dieser Obergrenze werden gezählt.
Für häufige Verletzer dieser Obergrenze wird \(max_q = avgcq\) gesetzt,
um diese häufiger zu bestrafen,
damit sie ihre Senderate herunterfahren.

Statt die Queue-Länge zu betrachten,
wird also für Flows gezählt, wie oft Flows Penalties erhalten.
Das Problem hierbei ist jedoch, dass die Router jetzt per-Flow State handlen müssen,
was nicht praktikabel ist.

\subsection{CHOKE}

Statt per-Flow State zu halten, werden eingehende Pakete
jetzt mit zufällig gewählten Paketen verglichen, die schon in der Queue sind.
Wenn zwei Pakete die gleiche Flow ID haben, wird das Paket verworfen.
Dies soll besonders häufig auftreten,
wenn ein Flow aggressiv ist, d.\,h. auf Congestion nicht (ausreichend) reagiert.
Ansonsten wird weiter RED (bzw. eine RED-Variante) angewendet.
Hier spart man sich zwar per-Flow State,
ist aber unfair gegenüber Szenarien mit nur wenigen Flows,
da hier auch ausreichend reaktive Flows wahrscheinlich getroffen werden.

\subsection{Adaptive RED}

Ziel von ARED war, Parameter von RED automatisch anzupassen,
um es leichter für Administratoren einstellbar zu machen.
Mit einer AIMD-Policy wird \(P_{max}\) periodisch langsam angepasst.
Weiter wird fix \(max_{th} = 3 \cdot min_{th}\) und
und \(w_q = 1 - \exp\left(- link\_capacity^{-1}\right)\) gesetzt.
Damit soll die Geschwindigkeit von RED unabhängig von der Linkgeschwindigkeit werden.
\(P_{max}\) wird regelmäßig aktualisiert, um möglichst groß zu sein,
aber gleich zeitig möglichst selten die Queue vollaufen zu lassen.

\subsection{Stabilized RED}

Stablized RED eliminiert die Glättung mittels EWMA (also \(avgq\) und \(w_q\)).
Die Wahrscheinlichkeit \(P_m\) ist eine Funktion \(f\),
die Abhängig von der aktuellen Queue-Länge,
der Anzahl aktiver Flows und dem Rang des aktuellen Flows ist.
Es wird eine Zombie-Liste geführt,
in der \(K\) gesehene Flows mit ihren Hit-Countern gespeichert werden.
Trifft ein Paket ein,
wird ein Zombie Flow zufällig gewählt
und falls die Flows matchen, wird der Hit Counter erhöht.
Ansonsten wird der Zombie mit einer gewissen Wahrscheinlichkeit
durch den neu gesehenen Flow ersetzt.

Dadurch werden Flow statistisch basierend auf ihrer Trefferwahrscheinlichkeit gezählt.
Der Rang des Flows ist der Hit Counter, wenn ein Match auftritt.

\subsection{BLUE}

BLUE versucht, vergangene Erkenntnisse bei der Nutzung von RED zu nutzen.
Insbesondere soll es nicht mehr notwendig sein,
Parameter anpassen zu müssen.
Weiter sollen Effekte von durchschnittlichen Queue-Fluktuationen auf AQM vermieden werden.

Pakete werden mit einer Wahrscheinlichkeit \(P_m\) markiert.
Diese ist wieder eine Funktion in Packet Loss und Link Idle Events.
Tritt Packet Loss auf und ist die Zeit seit der letzten Ankunft
länger her als eine gewisse Freeze-Zeit, setze \(P_m = P_m + d_1\).
Ist der Link idle und ist die Freeze-Zeit überschritten,
setze \(P_m = P_m - d_2\).
Dabei wird \(d_1 >> d_2\) gewählt,
sodass bei Congestion die Reaktion schneller erfolgt als wenn idle ist.

Die Abwandlung SFBLUE erweitert BLUE wiederum um Ideen von SFQ und Fair Queueing,
um zwischen verschieden intensiven Flows unterscheiden zu können.

\subsection{CoDel -- Controlled Delay}

Die Adaptionen von RED haben gezeigt,
dass RED als Ansatz nicht wirklich zielführend ist.
Ein weit verbreitetes AQM-Verfahren ist \cdefname{CoDel}\index{CoDel}.
Anstatt sich Queue-Längen anzuschauen,
wird beobachtet, wie lange Pakete brauchen, um durch die Queue zu gehen.
Das funktioniert unabhängig von der Anzahl der Queues
und mit variablen Link-Raten (bspw. bei Funkverbindungen).
Ziel ist die Überwachung,
wie lange die minimale Queue-Länge über einem gewissen Threshold bleibt.
Dies ist zwar weniger aussagekräftig als die durchschnittliche minimale Queue-Länge,
ist aber ausreichend für AQM und leichter zu berechnen.

Beobachtet wird die \cdefname{Sojourn time}\index{Sojourn Time},
also die Zeit, wie lange eine Paket vom Empfang am Input Port
bis zum Versand am Output Port benötigt.
Dies lässt sich sowohl mit einer einzelnen als auch mit mehreren Queues messen und
funktioniert für variable Linkraten.
Zudem ist die Sojourn Time einfach zu messen und leicht implementierbar.

Bei steigender Senderate von TCP Flows steigt die Sojourn Time.
Wenn die Sojourn Time einen Threshold übersteigt,
wird ein Paket gedroppt und nach einem gewissen Intervall wieder
die Sojourn Time geprüft.
Je länger die Sojourn Time zu groß ist,
um so geringer werden die Intervalle, in denen geprüft und gedroppt wird.

Das Intervall wird als \(\frac{Interval}{\sqrt{drop count}}\) berechnet.
Das soll dem TCP Flow Zeit geben, Congestion zu erkennen
und darauf zu reagieren.
Je länger jedoch die Sojourn Time ist, umso häufiger werden auch Pakete gedroppt
und der Drop Count steigt.
Wird der Drop Mode nur kurzzeitig verlassen,
wird sich außerdem die letzte (scheinbar gute) Drop-Rate gemerkt,

Wenn Pakete gedroppt werden, passiert dies außerdem am Kopf der Queue,
nicht am Ende.
Dies hat den Vorteil, dass TCP schneller die Congestion erkennt
und verspätete Pakete von Echtzeitanwendungen eher gedroppt werden als solche,
bei denen es sich tatsächlich noch lohnt, dass sie ankommen.

CoDel hat signifikant weniger Konfigurationsmagie.
Die einzigen Parameter sind das Intervall (welches größer als eine RTT sein sollte),
der Target-Delay (Maximum von ca. 1--2 Paketen an Queue oder 5\,\% der Worst Case RTT)
Die Drop-Rate folgt einer Inverse-Square-Root Progression
und damit zu einem linearen Anstieg der Drops pro RTT.

CoDel kann wieder mit SFQ kombiniert werden
und ist so als fq\_codel in Linux-Distributionen als Queueing-Disziplin verfügbar.
Dies erlaubt es,
verschiedene Traffic-Klassen fair zu behandeln
und neuen Flows einen kleinen Vorsprung gegenüber langlebigeren Flows zu geben.

Ein weiterer Vorteil von CoDel liegt in der Messung.
Da die Sojourn-Time pro Paket gemessen wird,
muss für die Berechnung nicht die gesamte Queue blockiert werden.
