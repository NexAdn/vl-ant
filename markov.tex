\chapter[Markov-Prozesse]{Markov-Prozesse \cite{rosberg_telematik_2023}}

Die folgende Einführung in Markov-Prozesse basiert vorrangig aus dem Material
der Vorlesung „Telematik 2 / Leistungsbewertung“ \cite{rosberg_telematik_2023}.

\section{Einführung}

Markov-Prozesse sind stochastische Prozesse und ein sehr mächtiges Mittel
in der Leistungsbewertung und Modellierung von Netzwerken.
Innerhalb von Netzwerken gibt es dabei viele Knoten,
die ein Warteschlangensystem beinhalten.
Das Verhalten der Warteschlangen wird dabei maßgeblich davon beeinflusst,
mit welcher Rate Pakete in die Warteschlange gelegt und
wie schnell die Pakete in der Warteschlange abgearbeitet werden.
Zur Analyse dieses Verhaltens werden solche Warteschlangensysteme als
sog. \emph{Markov-Ketten} modelliert.
Dies erlaubt die Berechnung von Wahrscheinlichkeiten bestimmter Warteschlangenzustände
(d.\,h.~Füllstände der Warteschlangen)
und damit auch die Ableitung von Leistungsmerkmalen der Systeme.

Ein stochastischer Prozess wird als \cdefname{Markov-Prozess}\index{Markov-Prozess}
bezeichnet,
wenn er die \cdefname{Markov-Eigenschaft}\index{Markov-Eigenschaft} besitzt.
Dies besagt, dass Ereignisse unabhängig sind,
also das zukünftige Verhalten des Systems nach Zeitpunkt \(t\)
nur vom Zustand \(X\left(t\right)\) abhängt und \emph{keinen} vorigen Zuständen
(\(X\left(\tau\right), \tau < t\)).

Dies hat zwangsläufig zur Konsequenz,
dass die Inter-Arrival- und Servicezeiten exponentiell verteilt und
die Ankunfts- und Serviceereignisse Poisson-verteilt sind.

\section{Markov-Ketten}

Markov-Ketten und Markov-Prozesse besitzen einen diskreten Zustandsraum
(z.\,B.~Anzahl Pakete in einer Queue)
und einen stetigen Zeitraum
(es gibt keinen festen Takt, in dem Pakete ankommen können,
sondern dies kann jederzeit passieren).
Das zukünftige Verhalten des Markov-Prozesses ist dabei vollständig
durch den aktuellen Zustand beschreibbar.

Markov-Ketten werden gerne genutzt,
um Zustandswahrscheinlichkeiten nach Erreichen einer \emph{Steady Phase}\index{Steady Phase}
zu analysieren.
Es wird also angenommen, dass der Prozess ausreichend lang lief,
sodass die Wahrscheinlichkeit, dass sich das System in einem bestimmten Zustand befindet,
weder vom Ausgangszustand noch von der aktuellen Zeit abhängt.
Ein solcher stationärer Zustand muss existieren, wenn Markov-Ketten
\begin{itemize}
	\item \cdefname{positiv rekurrent}\index{positiv rekurrent} sind
		(d.\,h.~jeder Zustand ist in endlich viel Zeit erreichbar),
	\item \cdefname{irreduzibel}\index{irreduzibel} sind
		(d.\,h.~jeder Zustand ist von jedem anderen Zustand aus erreichbar) und
	\item \cdefname{endlich}\index{endlich!Markov-Kette} sind.
\end{itemize}
Solche Ketten und Zustände werden dann als \cdefname{ergodisch}\index{ergodisch}
bezeichnet
und sie erreichen nach einer transienten Phase (\(t \to \infty\))
einen Steady State.

\section{Analyse einer Markov-Kette}

Betrachte das einfache Bespiel einer Markov-Kette in \autoref{img:markov-chain-example}.
Dies könnte beispielsweise ein System aus zwei Queues
mit zwei Paketen sein, die zwischen diesen Systemen hin- und hergeschoben werden.
Es können also zu jedem Zeitpunkt diese zwei Pakete in verschiedenen Queues liegen
(also entweder beide in der ersten Queue, beide in der zweiten Queue
oder ein Paket in einer und eines in einer anderen Queue).
Die Zustände repräsentieren hierbei alle Kombinationen von Füllständen der Queues,
während \(\mu_1\) und \(\mu_2\) die Wahrscheinlichkeiten angeben,
dass ein Zustandswechsel stattfinden.
\(\mu_1\) ist dabei die Wahrscheinlichkeit, dass der erste Knoten ein Paket verschickt.
Dies führt dazu, dass der zweite Knoten ein Paket mehr in der Queue hat,
also einen Zustand weiter nach rechts gegangen wird.
\(\mu_2\) ist umgekehrt die Wahrscheinlichkeit, dass der zweite Knoten ein Paket verschickt
und in den Zustand weiter links gegangen wird.
Zusätzlich gibt es auch für jeden Zustand die Wahrscheinlichkeit,
dass sich der Zustand nicht ändert.
Diese lässt sich durch Differenzbildung berechnen und ist nicht weiter aufgeführt.

\begin{figure}[!htp]
	\centering
	\begin{tikzpicture}[>=stealth, node distance=3cm]
		\node[state] (q20) at (0, 0) {\(p\left(2, 0\right)\)};
		\node[state, right of=q20] (q11) {\(p\left(1, 1\right)\)};
		\node[state, right of=q11] (q02) {\(p\left(0, 2\right)\)};

		\draw[->] (q20) edge[bend right] node[below] {\(\mu_1\)} (q11);
		\draw[->] (q11) edge[bend right] node[below] {\(\mu_1\)} (q02);

		\draw[->] (q02) edge[bend right] node[above] {\(\mu_2\)} (q11);
		\draw[->] (q11) edge[bend right] node[above] {\(\mu_2\)} (q20);
	\end{tikzpicture}
	\caption{
		Einfaches Zustandsdiagramm eines Markov-Prozesses
	}
	\label{img:markov-chain-example}
\end{figure}

Zur Analyse solcher Markov-Ketten können nun Kolmogorovs Gleichungssystem aufgestellt werden.
Genutzt wird dabei, dass die Wahrscheinlichkeit,
den Zustand zu betreten, gleich der Wahrscheinlichkeit ist, den Zustand zu verlassen.
Dazu wird für jeden Zustand eine Gleichung aufgestellt,
bei der von den Wahrscheinlichkeiten,
von einem (anderen) Zustand in diesen Zustand zu wechseln,
die Wahrscheinlichkeiten, diesen Zustand zu verlassen,
subtrahiert werden.
Dabei wird auch jeweils die Wahrscheinlichkeit,
in einem entsprechenden Zustand zu sein, multipliziert:

\begin{align}
	\text{Zustand (2, 0):} &&
	p_{\left(1, 1\right)} \cdot \mu_2 - p_{\left(2, 0\right)} \cdot \mu_1 &= 0\\
	\text{Zustand (1, 1):} &&
	p_{\left(2, 0\right)} \cdot \mu_1 + p_{\left(0, 2\right)} \cdot \mu_2
	- p_{\left(1, 1\right)} \cdot \left(\mu_1 + \mu_2\right) &= 0\\
	\text{Zustand (0, 2):} &&
	p_{\left(1, 1\right)} \cdot \mu_1 - p_{\left(0, 2\right)} \cdot \mu_2 &= 0\\
	\shortintertext{
		Zum Schluss wird noch ausgenutzt,
		dass sich das System jederzeit in einem der Zustände befinden muss,
		d.\,h.~dass die Summe der Wahrscheinlichkeiten,
		in einem der Zustände zu sein, immer \(1\) sein muss:
	}
	p_{\left(2, 0\right)} + p_{\left(1, 1\right)} + p_{\left(0, 2\right)} &= 1
\end{align}

Dieses lineare Gleichungssystem lässt sich lösen und ergibt die Wahrscheinlichkeiten,
sich in einem der Zustände zu befinden.
Diese Wahrscheinlichkeiten können dann bspw. genutzt werden,
um zu ermitteln, wie ausgelastet die Knoten im Netzwerk sind
(d.\,h.~wie viel Prozent der Zeit sie arbeiten).

\section{Eindimensionale Geburts- und Sterbeprozesse}

Ein Markov-Prozess ist ein eindimensionaler Geburts- und Sterbeprozess,
wenn nur Zustandsübergänge zwischen „benachbarten“ Zuständen möglich sind
(z.\,B.~wenn Zustände die möglichen Füllstände einer Warteschlange sind).
Die zwei Wahrscheinlichkeiten eines solchen Prozesses sind:
\begin{description}
	\item[Geburtswahrscheinlichkeit] \(\lambda_k\), die die Wahrscheinlichkeit angibt,
		von Zustand \(k\) nach \(k+1\) überzugehen.
	\item[Sterberate] \(\mu_k\), die die Wahrscheinlichkeit angibt,
		vom Zustand \(k+1\) nach \(k\) überzugehen.
\end{description}

Hierfür gibt es dann die angepasste Chapman-Kolmogorov-Gleichung:
\begin{align}
	\frac{\dd}{\dd t}p_k\left(t\right) &= \underbrace{
		p_{k+1}\left(t\right) \mu_{k+1} +
		p_{k-1}\left(t\right) \lambda_{k-1}
	}_{\text{Transitions to state } k}
	- \underbrace{
		p_k\left(t\right) \cdot \left(\lambda_k + \mu_k\right)
	}_{\text{Transitions from state } k}
	& \text{Für } k > 0\\
	\frac{\dd}{\dd t} p_0\left(t\right) &=
	p_1\left(t\right) \mu_1 - p_0\left(t\right) \lambda_0
	& \text{Für } k = 0\\
	\shortintertext{Normalisierungs-Bedingung:}
	\sum_{k \in Z}p_k\left(t\right) &= 1
\end{align}

In einem solchen eindimensionalen Geburts- und Sterbeprozess existiert
globales Gleichgewicht für Zustand \(k\),
wenn Übergänge von und zu benachbarten Zuständen im Gleichgewicht sind:
\begin{align}
	0 &= p_{k+1}\mu_{k+1} + p_{k-1}\lambda_{k-1} - p_k\left(\lambda_k + \mu_k\right)
	& k > 0\\
	0 &= p_1\mu_1 - p_0\lambda_0 & k = 0\\
	\sum_{k\in Z} p_k &= 0
\end{align}

Im Steady State ergibt sich bei globalem Gleichgewicht die Wahrscheinlichkeit,
dass sich das System im Zustand \(k\) befindet, als:
\begin{align}
	p_k &= p_0 \cdot \prod_{i=0}^{k-1} \frac{\lambda_i}{\mu_{i+1}} & k > 0
\end{align}

\section{Das M/M/1-System}

Das M/M/1-Queueing-System modelliert einen einzelnen Knoten in einem Netzwerk.
Dabei wird eine unendlich große Queue angenommen
(also auch unendlich viele Zustände).
Die ist zwar in der Praxis nicht der Fall, macht jedoch die Analyse einfacher.
In der Praxis würde eine zu volle Queue jedoch zu Paketverlust führen.
Somit kann eine in der Theorie sehr große Queue ein Indikator dafür sein,
dass sich ein entsprechendes System in der Praxis sehr instabil verhalten würde.

Das M/M/1-System ist ein eindimensionaler Geburts- und Sterbeprozess,
bei dem \(\lambda = \lambda_0 = \lambda_1 = \dotsc\)
und \(\mu = \mu_0 = \mu_1 = \dotsc\),
es also vom Zustand unabhängige, unveränderliche Geburts- und Sterberaten gibt.
Die Geburtsrate wird hier als „Ankunftsrate“ bezeichnet,
die in der Praxis der Ankunft eines Paketes in der Queue entspricht.
Die Sterberate ist die Servicerrate/Bedienrate und gibt an,
wie schnell das System Pakete aus der Queue verschicken kann.

Die Last des Systems wird als \(\rho := \frac{\lambda}{\mu}\) definiert
und \(\rho < 1\) muss gelten, damit das System einen Steady State
erzielen kann.
Für die Wahrscheinlichkeiten \(p_k\) gilt dann:
\begin{align}
	p_0 &= 1 - \frac{\lambda}{\mu} = 1 - \rho\\
	p_k &= p_0 \cdot \left(\frac{\lambda}{\mu}\right)^k & k > 0\\
	&= \left(1 - \rho\right)\rho^k
\end{align}
Dies ist die Wahrscheinlichkeit, dass \(k\) Pakete im System sind.
Da \(\rho < 1\), ist diese für steigende \(k\) monoton fallend.

Weiter lässt sich die durchschnittliche Anzahl Pakete \(k\) im System ermitteln:
\begin{align}
	k &= \frac{\rho}{1-\rho}\\
	\shortintertext{
		Die durchschnittliche Anzahl Pakete \(L\) in der Warteschlange ist:
	}
	L &= \frac{\rho^2}{1-\rho}
\end{align}
Beide Werte steigen für wachsendes \(\rho\) stark an.
Eine hohe Last führt also zu vollen Warteschlangen und
irgendwann auch zu Überlast (sprich: Paketverlust).
Für die durchschnittliche Antwortzeit \(T_v\) und Wartezeit \(T_w\) gilt:
\begin{align}
	T_v &= \frac{1}{\mu - \lambda}\\
	T_w &= \frac{1}{\lambda}\cdot \frac{\rho^2}{1 - \rho}
\end{align}
Auch hier führt eine hohe Last dazu, dass \(T_v\) und \(T_w\) stark ansteigen.
