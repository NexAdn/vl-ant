LTX = latexmk -xelatex -file-line-error -interaction=nonstopmode -halt-on-error

PDFS = vl.pdf

UPLOADS = $(foreach f,$(PDFS),$(f)=fach/ant/ss2024/$(f))

.PHONY: all upload
all: $(PDFS)

%.pdf: %.tex $(wildcard chap*.tex markov.tex botd.tex)
	$(LTX) $<

upload: $(PDFS)
	@/home/nex/projects/dwup/dwup.py \
		-u https://wikiin.de \
		-U nex \
		-p $(shell pass wikiin.de/nex | head -n1) \
		--file $(UPLOADS)
