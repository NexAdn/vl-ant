\chapter{Transport Layer Evolution}

\section{TCP Congestion Control}

Ein Hauptproblem von TCP Congestion Control ist die Designentscheidung,
dass es grundsätzlich bis zur Lastgrenze skaliert,
da es die Sendeleistung verstärkt, bis Pakete verloren gehen.
Das hat bereits bei der Pufferung von Paketen zu Problemen geführt,
da Puffer erst vollaufen müssen,
bis TCP die Sendeleistung verringert.
Zudem sind Paketverluste gerade bei Funknetzen nicht mehr ausschließlich
auf Stausituationen zurückzuführen.
So könnte es bei verlustbehafteten Links sogar nützlich sein,
mehr Daten zu senden.
Ein weiteres Problem ist die Beschränkung der normalen Windowgröße auf 16 Bit,
was zu Problemen bei der Übertragung mit hohem BDP (hohe Latenz, hohe Bandbreite) führt.
Außerdem braucht TCP durch AIMD bei hohen Bandbreiten auch sehr lange,
bis die volle Sendeleistung erreicht wurde.
Das macht schon allein die Übertragung von 10 Gb/s schwer.

Statt TCP zu verändern, wurden um TCP herum Workarounds gebaut.
So gab es Ansätze, TCP an WLAN-Links zu terminieren
und ein anderes Transportprotokoll wie I-TCP, METP oder SNOOP über den WLAN-Link zu sprechen.
Wie in \autoref{sec:buffers} gezeigt,
werden auch Queueing-Strategien um TCP herum entworfen.
Andere Strategien sind die Einführung von \emph{PAUSE-Frames}\index{Pause-Frame},
die auf Layer 2 signalisieren,
dass TCP-Verbindungen ihre Sendeleistung reduzieren sollen,
oder \emph{WAN-Optimierer}\index{WAN-Optimierer},
die TCP-Verbindungen beobachten und bspw. Retransmissions früher durchführen,
ACKs proaktiv senden oder auch ACKs verwerfen, wenn viele gesendet werden.

TCP hingegen wurde so gut wie nicht verändert.
Daher stellt sich die Frage, ob es auch etwas besseres als TCP gibt.
So gibt es schon seit vielen Jahren Arbeit an der Verbesserung von
TCP Congestion Control.
Dabei muss beachtet werden,
dass Abwärtskompatibilität gegeben ist.
Die Veränderung von TCP ist nur sehr langsam möglich.
Auch die bei der IETF doch sehr schnell publizierten RFCs
brauchen sehr lange,
bis sie von Herstellern der Betriebssysteme tatsächlich umgesetzt werden.

Insbesondere dürfen Änderungen an TCP nicht mit der Fairness spielen.
TCP kann prinzipiell dahingehend modifiziert werden,
dass man selbst die Sendeleistung so hochfährt,
dass alle anderen Verbindungen die Sendeleistung reduzieren.
Diese Fairness zu stören, führt also dazu,
dass andere Verbindungen keine Bandbreite mehr bekommen.
Eine weit verbreitete Implementierung von TCP Congestion Control,
die in vielen Betriebssystemen verbreitet ist,
ist \emph{TCP CUBIC}\index{TCP CUBIC}.
Andere Entwicklungen sind bspw. CTCP und BBR.
Für Rechenzentren werden teilweise auch spezialisierte Protokolle entwickelt,
die zusätzliche Anforderungen an die Switching-Hardware haben.

\subsection{TCP SACK}

\emph{Selective Acknowledgements}\index{Selective Acknowledgement}
versuchen, das Go-Back-N-Problem zu lösen,
was bei Paketverlust auftritt.
Statt ständig ACKs zu senden, die bei Paketverlust
zu fast vollständiger Retransmissions aller Segmente ab dem Paketverlust führen können,
wird genau angegeben, bis wohin Pakete empfangen wurden
und welche Pakete nach einem nicht empfangenen Paket bereits empfangen wurden.
Dazu werden SACK-Optionen an das TCP-Paket angefügt.
Bei Paketverlust weiß der Sender so, dass nur einzelne Segmente neu verschickt werden müssen.

Ein Problem mit SACK ist,
dass bei der Implementierung viele Fehler gemacht wurden (Stand 2011).
Solche fehlerhaften Implementierungen führen zu starken Performanceeinbrüchen.
Häufig führen solche Fehler dazu, dass sich der Zustandsautomat von SACK verklemmt.
Als Fallback werden die SACKs gelöscht.

Ein weiteres Problem lässt sich beim Durchsatz mit zunehmendem Paketverlust sehen.
Messungen haben gezeigt,
dass die Benutzung von SACK kaum Verbesserungen bei zunehmenden Paketverlust liefert.

\subsection[Compound TCP]{Compound TCP (C-TCP)}\index{C-TCP}\index{TCP!Compund}

C-TCP wurde von Microsoft entwickelt
als Reaktion auf darauf, dass bei steigender Bandbreite die Latenz nicht gesunken ist.
Bei hoher Latenz ist das Problem,
dass TCP die Senderate nur langsam hochfährt.

C-TCP arbeitet dafür mit zwei Congestion Windows.
Einerseits gibt es das normale Loss-basierte cwnd,
aber jetzt zusätzlich noch ein delay-basiertes Window, das dwnd,
welches sich schnell erhöht,
wenn kurze Delays beobachtet werden
und sich anschließend auf 0 reduziert,
wenn der Delay steigt.
So reduziert sich bei steigender Queue-Auslastung
das Window, um ein stabiles Steady-State-Verhalten zu erzielen.
In der Slow-Start-Phase
Nach Erreichen der AIMD-Phase wird Queueing gemessen.
Durch zunächst geringen Queuing-Delay steigt das dwnd
und sinkt nach gestiegenem cwnd wieder ab,
sodass C-TCP fair zu Reno bleibt.
Damit kann C-TCP schneller die Geschwindigkeit hochfahren als Reno.
C-TCP zeigt insbesondere bei geringem und mittleren Packet Loss einen besseren Durchsatz.
Bei hohem Packet loss ist C-TCP ähnlich schlecht wie Reno.


\subsection[CUBIC]{CUBIC \cite{rosberg_advanced_2024, peterson_tcp_2022}}

TCP CUBIC\index{CUBIC} ist eine Weiterentwicklung des älteren Algorithmus BIC
und zielt darauf ab,
das Window je nach Situation schneller oder langsamer zu öffnen.
Bei BIC wurde dafür binäre Suche benutzt,
wobei beim Erkennen von Packet Loss (etwa bei \(cwnd = cwnd_{max}\))
das Congestion Window um einen Faktor \(\beta\) verkleinert wird.
Mit jeder erfolgreichen Iteration an versendeten Paketen
wird das neue Congestion Window in die Mitte zwischen dem alten cwnd
und \(cwnd_{max}\) gesetzt,
d.\,h. \(cwnd = cwnd_{old} + \frac{1}{2} \cdot \left(cwnd_{max} - cwnd_{old}\right)\).
So nähert ähnlich zu binärer Suche das neue Congestion Window
an \(cwnd_{max}\) zunächst schnell und später langsam asymptotisch an.
Ist \(cwnd\) nah genug an \(cwnd_{old}\), nimmt BIC an,
dass sich die Netzwerkbedingungen geändert haben
und versucht, \(cwnd\) über \(cwnd_{max}\) hinaus zunächst langsam,
später schneller zu erhöhen,
um ein neues \(cwnd_{max}\) zu finden.

CUBIC wandelt BIC in verschiedenen Aspekten ab.
Zunächst wird eine kubische Funktion eingesetzt,
um das neue \(cwnd\) anhand der verstrichenen Zeit seit dem letzten Congestion Event
zu berechnen,
statt auf ACKs zu warten:
\begin{align*}
	cwnd &= C\left(t - \sqrt[3]{cwnd_{max} \cdot \beta / C}\right)^3 + cwnd_{max}
\end{align*}
So können Flows mit langer RTT auch mit Flows konkurrieren, die kurze RTT haben,
da die Veränderungsgeschwindigkeit des Congestion Window
nicht mehr abhängig von der RTT ist.

CUBIC erzielt deutlich höheren Durchsatz gegenüber Reno und
lässt dabei Reno-Flows nicht komplett verhungern.
Startet ein neuer Flow verzögert zu einem alten Flow,
der bereit die maximale Bandbreite einnimmt,
benötigt CUBIC durch die hohen Paketraten des alten Flows
aber sehr lange, um die neuen Flows hochzuregeln,
da sehr häufig Paket Loss auftritt
und dadurch nur langsam das Probing für mehr Bandbreite einsetzt.

\subsection{BBR}\index{BBR}

BBR steht für \emph{Bottleneck Bandwidth and Round-trip} propagation time
und ist ein von Google entwickeltes Congestion Control Verfahren.
Hauptziel ist Geschwindigkeit.
Dazu soll Buffer Bloat von TCP eliminiert werden,
da dieser schnelle Reaktionen auf Stau verhindert.
Statt Pakete zu verlieren,
soll BBR Stau schon erkennen, sobald er sich bildet.
Das Verfahren ist also nicht Packet-Loss-basiert, sondern Congestion-basiert.

Das Verfahren sendet Daten durch das Netz und prüft,
ob durch höhere Senderaten auch höhere ACK-Raten erzielt werden.
Zusätzlich wird auf Delay-Informationen zurückgegriffen.
Das Verfahren wird insbesondere nicht durch ACKs geclockt, sondern nur gepaced.

Die Idee von BBR entspringt Beobachtungen von RTT und Durchsatz bei steigender Senderate.
Solange nichts gequeuet wird, bleibt die RTT etwa konstant,
während die Delivery Rate steigt.
Wird irgendwann das BDP erreicht, kann nicht mehr übertragen werden.
Dadurch werden Pakete gequeuet und die RTT steigt.
Irgendwann sind auch die Puffer voll und es tritt Packet Loss auf.
Der optimale Arbeitspunkt ist aber bereits beim BDP erreicht,
wo die RTT minimal und die Delivery Rate maximal ist.
Loss-basierte Ansätze hingegen arbeiten jenseits dieses Arbeitspunktes.

BBR versucht nun zu Beginn des Flows, mit Slow Start den Arbeitspunkt zu finden.
Sobald die ACK-Rate nicht mit der Senderate steigt,
ist der Betriebspunkt erreicht und BBR steigt immer langsamer.
Da BBR jetzt aber schon Stau verursacht (zu viele Pakete sendet),
wird jetzt die Senderate wieder zurückgefahren.
Dazu wird der verursachte Stau abgeschätzt und die Senderate passend (langsam) reduziert,
um genau den selbst erzeugten Stau zu eliminieren.
Dies wird regelmäßig wiederholt, um den Arbeitspunkt neu zu ermitteln.
Bei richtiger Parameterwahl lässt sich BBR auch Fair zu CUBIC einstellen.

BBR zeigt auch bei hohen Paketverlustraten deutlich besseren Durchsatz als CUBIC.
Während bereits bei \(0,1\,\%\) Verlustrate der Durchsatz von CUBIC einbricht,
bleibt BBR lange nah am Optimum.
Wird in einem System mit viel Packet Loss BBR parallel zu CUBIC eingesetzt,
wird jedoch CUBIC verhungern, womit also keine Fairness mehr gegeben ist.

Messungen mit verschiedenen Queueing-Verfahren haben jedoch gezeigt,
dass BBR beim Einsatz von AQM-Verfahren starke Bandbreitenschwankungen hat.
Das Problem ist hierbei,
dass mehrere BBR-Flows parallel versuchen, die Situation im Netzwerk zu erspüren.
Da dies parallel ausgeführt wird,
fließen die Einflüsse aus den Messungen der anderen Flows mit in das Ergebnis ein
und sie beeinflussen sich gegenseitig.
Das führt zu Schwankungen in der RTT und damit dem ermittelten Betriebspunkt.
Dadurch wird BBR bei einer großen Anzahl Flows instabil.
Kleine Puffergrößen führen zu um Größenordnungen mehr Retransmits,
da weniger Platz zum Ermitteln des Betriebspunktes bleibt.
Da Packet Loss als Zeichen eines verlustbehafteten Links angesehen wird,
wird die Senderate nicht wirklich heruntergefahren,
sondern weiter mit (zu) hoher Rate gesendet.
Weitere Probleme treten auf, wenn sich BBR im Betriebspunkt verschätzt.
So werden CUBIC-flows teilweise verdrängt, wenn diese nach BBR-Flows gestartet werden.
Durch diese hohe Instabilität, die mit BBR verbunden ist,
wird dieses Verfahren in der Praxis nicht eingesetzt.

\subsection{Excursion: Why is everybody worried about fairness?}

Die bisher gezeigten Verfahren versuchen alle, „fair“ zu bleiben.
Dies entspringt den Entwicklungen der 80er Jahre.
Dort wurde nicht der Durchsatz optimiert,
sondern die \emph{Network Power}\index{Network Power} \(P\),
das Verhältnis von Gesamtdurchsatz zu durchschnittlichem Delay.
Dies soll durch lokal beobachtbare Effekte optimiert werden,
bspw. durch Messung der RTT oder Übermittlung von Metriken von Routern an Endgeräte.
Leider lässt sich die Network Power trotz dieses Wissens nicht optimal einstellen.

Um \(P\) zu maximieren,
wird versucht, die 1. Ableitung nach der Datenrate \(0\) zu setzen.
Dabei ergibt sich eine Differentialgleichung,
die Fordert, dass das Verhältnis aus Delay und Durchsatz
gleich der Ableitung des Delays nach dem Durchsatz ist.
Modelliert man Links als M/M/1-System ergibt sich eine Gleichung für \(D\).
Durch Ableitung lässt sich ein Wert für den Durchsatz, nämlich die halbe Linkkapazität.

Es zeigt sich, dass im Fall von zwei Flows die Network Power sogar besser wird,
wenn einer der Flows verhungert.
Die Metrik selbst ist also nicht gut geeignet als Optimierungsziel.
Es zeigt sich sogar, dass alle Kritierien,
die auf Gesamtdurchsatz oder durchschnittliche Latenz basieren,
nicht dezentralisiert laufen kann.
Daher wurde sich darauf geeinigt auf Fairness zu setzen.
Für Systeme, die global optimieren (z.\,B. SDN) wäre es jedoch weiter möglich,
Durchsatz oder Latenz für In-House-Traffic zu optimieren.

\section{Multipath TCP}

Es gibt heutzutage insbesondere durch Mobilgeräte häufig die Situation,
dass Geräte mehrere IP-Adressen und Uplinks haben.
Herkömmliche TCP-Verbindungen können jedoch immer nur einen Link nutzen.
Es ist aber wünschenswert, beide Links parallel nutzen zu können,
um mehr Durchsatz zu erzielen.
Durch die Nutzung mehrerer Links ließe sich auch ein Failover realisieren,
wenn ein Link während der Verbindung ausfällt.
Auch die in Rechenzentren übliche Fat Tree Topologie erlaubt
die Nutzung mehrerer unabhängiger Links zwischen je zwei Servern.

Ziel von \emph{Multipath TCP}\index{Multipath TCP} (MPTCP) ist die Arbeit
in verschiedenen Szenarien mit mehrere IP-Adressen
ohne Source Routing.
Multipath TCP soll weiterhin vollständig abwärtskompatibel zu den bestehenden APIs
und Middleboxen (Firewalls, NICs, NAT, etc.) sowie
zu „normalen“ (nicht-Multipath) TCP sein.

MPTCP öffnet mehrere reguläre TCP-Verbindungen,
die über eine MPTCP-Ab\-strak\-tions\-schicht wie eine (einzelne) reguläre TCP-Verbindung
für die Applikation aussieht.
Um den Support für MPTCP zu ermitteln,
wird im SYN, SYNACK und darauffolgenden ACK das Flag mitgegeben,
dass MPTCP-Support vorhanden ist.
Das Flag im dritten Paket wird (scheinbar redundanterweise) übertragen,
damit einerseits der Verbindungsaufbau serverseitig weiter stateless möglich ist
und erkannt werden kann, ob Firewalls möglicherweise die MPTCP-Optionen filtern.

Beim Handshake werden zusätzlich Schlüssel ausgetauscht.
Diese stellen sicher, dass neue Verbindungen zur alten zugeordnet werden können
und Netzwerkteilnehmer abseits des ersten Netzwerkpfades
keine Verbindungen (unautorisiert) hinzufügen können.
Um neue Verbindungen hinzuzufügen, müssen neue TCP-Verbindungen mit Join-Option,
Nonce und einem HMAC zur Authentifizierung eröffnet werden.
Damit lassen sich beliebig viele TCP-Verbindungen einer MPTCP-Verbindung hinzufügen.

Um mit Systemen wie Firewalls und NAT umzugehen,
die keine beidseitigen Verbindungsaufbauten unterstützen,
können Geräte mit TCP-Optionen eigene Adressen an die Gegenstelle übermitteln,
um diese zu einem Verbindungsaufbau zu bewegen.

Um Probleme mit Middleboxes zu umgehen, die den TCP-Datenstrom beeinflussen,
müssen alle Verbindungen der MPTCP-Verbindungen unabhängig betrachtet werden.
So dürfen bspw. ACKs von einem Flow nicht genutzt werden, um Pakete anderer Flows zu ACKen.
Dafür wird über Optionen ausgetauscht,
welche Sequenznummernräume in welchen Verbindungen benutzt werden.
Damit wird der Empfänger im Vorfeld darüber informiert,
welche Sequenznummern über welche Verbindungen laufen.
Im Falle des Verbindungsabbruchs bei einem Link ist es möglich,
Remappings (ggf. für immer) durchzuführen,
bspw. um einen Link aus der Verbindung auszuschließen.
Dennoch müssen auf dem (nicht mehr benutzten) Link weiter Retransmits durchgeführt werden,
bis die Verbindung richtig geschlossen wird, um Middleboxen nicht zu verwirren.

Congestion Control ist ein weiteres schwieriges Thema.
Da keine Kontrolle über die Pfade im Netzwerk für die Verbindungen besteht,
können Situationen auftreten, in denen zwei Unter-Verbindungen sich gegenseitig stören.
Durch viele Verbindungen breitet sich Congestion auch deutlich stärker im Netz aus.
Durch gegenseitige Beeinflussung der Flows kann es auch zu Schwingungen kommen,
bei denen sich zwei Subflows gegenseitig hoch- und herunterfahren.
Dies macht es erforderlich,
Congestion Control über mehrere Verbindungen hinweg laufen zu lassen.
Bestehende Verfahren für MPTCP Congestion Control
betrachten die Flows weitgehend unabhängig.

\emph{Loosely Coupled Subflows}\index{Loosely Coupled Subflows} (LIA)
arbeitet mit regulärer TCP Congestion Control,
begrenzt aber die Congestion Windows abhängig
von der Gesamtzahl Flows und dazugehöriger RTTs.
Dadurch wird verhindert, dass das schnelle Hochfahren eines Flows
nicht auch noch andere Flows mit hochfährt.
Load Balancing zwischen Flows findet dabei nicht statt.
Dieses Verfahren war trotzdem unfair gegenüber Reno.

Ein anderes Verfahren, \emph{Opportunistic Linked-Increases Algorithm}\index{Opportunistic Linked-Increases Algorithm} (OLIA)
addressiert das Problem der Pareto-Optima.
Das \(\alpha\) wird abhängig von der geschätzten Bandbreite für die verschiedenen Flows
positiv oder negativ.

\section{SCTP}

\emph{SCTP}\index{SCTP}
ist ein Transportprotokoll für zuverlässige nachrichtenorientierte Datenübertragung
und wurde ursprünglich für SS7 (eine Art Routingprotokoll für Telefonnetze) entwickelt.
Damit sollen die Vorteile von TCP mit der nachrichtenorientierten Natur von UDP
kombiniert werden.
Das Protokoll wurde 2000 im RFC 2960 spezifiziert und wird bis heute aktualisiert.
Damit ist es deutlich jünger als TCP und UDP.

Viele Probleme der anderen beiden Transportprotokolle sind in SCTP nativ gelöst:
\begin{itemize}
	\item Selective ACKs
	\item Multistreaming (statt mehrere Verbindungen parallel aufbauen zu müssen)
	\item Multihoming
	\item Heartbeats
	\item \emph{TLV Coding}\index{TLV Coding} (Type-Length-Value) von Extension Headern
	\item SYN Flood Protection
	\item Keine halboffenen Verbindungen
\end{itemize}

SCTP baut für eine Verbindung mehrere Streams auf,
zwischen denen sich auch Nachrichten überholen können
und die über eine SCTP-Verbindung per IP übertragen werden.
Auf der anderen Seite werden die Streams wieder aufgetrennt und in separate Puffer gepackt,
sodass ein voller Puffer in einem Stream den anderen Stream nicht stört.

Der Verbindungsaufbau findet mit einem Vier-Wege-Handshake
nach einem üblichen Request-Reply-Schema statt.
Damit werden Probleme mit Retransmits vermieden,
die bei einem Drei-Wege-Handshake auftreten,
da jetzt allein der Initiator der Verbindung für Retransmits zuständig ist
und der Server keinen Zustand halten muss,
bis der Initiator das zweite Paket geschickt hat.
Dies verhindert automatisch SYN-Flood-Angriffe.
Der Verbindungsabbau wird von einer der Parteien mit einem SHUTDOWN-Paket gestartet
und erfolgt ähnlich zu einem Drei-Wege-Handshake.

Identifiziert werden Verbindungen über eine 32-bit ID statt
über die Kombination aus IP-Adressen und Portnummern.
Einzelne Chunks von Daten sowie zusätzliche Protokollfunktionen
(z.\,B. Shutdown) werden über Chunk Header
an den Common Header angefügt.
Diese enthalten einen Type, Flags, eine Länge und
die zum Chunk dazugehörige Payload.
Zwei Bits im Chunk Type kodieren außerdem,
wie die Gegenseite Fehlerbehandlung bei unbekannten Chunk-Type handhaben soll.
Dies ermöglicht mehr Flexibilität bei der Abwärtskompatibilität.

In Data Chunks stehen Stream Identifier und Payload Protocol Identifier.
Diese ermöglichen die Zuordnung von Chunks zu Streams und
erlauben der Anwendungsschicht, den Protokolltyp per Identifier festzulegen
statt eigene Erkennungsmechanismen in das Anwendungsprotokoll einzubetten.

SCTP unterstützt Multihoming.
Dazu werden HEARTBEAT-Nachrichten auf verschiedenen Pfaden verschickt,
um zu prüfen, ob Pfade funktionsfähig sind.
Die Adressen dafür werden in der INIT-Sequenz ausgetauscht.
Kontrollnachrichten werden dennoch aus Robustheitsgründen
über nur einen Primärpfad verschickt,
der erst beim Ausfall des Pfades gewechselt wird.

SCTP ist aktuell kaum in Verwendung.
Hauptgrund hierfür ist, dass TCP nach wie vor (ausreichend gut) funktioniert.
Zudem müssen Anwendungsentwickler zunächst aktiv SCTP aktivieren.
Zudem wird auch Kernel-Support benötigt,
der in vielen Konfigurationen noch nicht aktiviert ist.
Daher benutzen Anwendungen, die SCTP benutzen müssen,
häufig Userspace-Implementierungen, die mit Raw-Sockets arbeiten.
Weiter ist NAT für SCTP noch nicht richtig spezifiziert,
was die Benutzung bei IPv4 Endkundenanschlüssen praktisch unmöglich macht,
ohne es bspw. in UDP (RFC 6951) zu kapseln.

\section{SPDY and HTTP/2}

HTTP ist die im Internet dominante Anwendung.
Durch steigende Datenmengen pro Request und die zunehmende Anzahl Verbindungen
pro aufgerufener Seite
führen dazu, dass eine effiziente Transportschicht signifikanten Einfluss
auf die Performance hat.

\emph{SPDY}\index{SPDY} ist eine von Google entwickelte Alternative zu HTTP
mit dem Ziel, die Ladezeiten von Webseiten zu halbieren.
Dabei wurden bspw. HTTP-Header komprimiert.
Das Protokoll wurde 2015 aufgegeben.

Die Ideen von SPDY wurde jedoch für \emph{HTTP/2}\index{HTTP2@HTTP/2} aufgegriffen.
Dabei wurde das Hauptproblem von HTTP/1.1 Pipelining angegangen,
nämlich Fehlverhalten mancher Anwendungen,
die dazu führen, dass Pipelining häufig deaktiviert wird.
Ein weiteres Problem ist Head-Of-Line Blocking,
was dazu führt, dass zuerst angefragte langsame Objeke übertragen werden müssen,
bevor schnelle, später angefragte Objekte, geladen werden können.
Daher werden mehrere TCP-Verbindungen (und teilweise TLS-Verbindungen) eröffnet,
was zu viel Aufwand führt.
Die Lösung ist, Ideen von SCTP wie Multi-Stream Support aufzugreifen
und auf TCP/TLS aufzubauen.

Dafür wird zunächst ein HTTP/1.1 Application Interface bereitgestellt,
um Abwärtskompatibilität zu gewährleisten.
Darunter wird dies jedoch von der Implementierung zu HTTP/2.0-Frames umgebaut.
Diese trennen u.\,A. Header von Daten.
Zusätzlich können mehrere Streams eröffnet werden,
die in der TCP-Verbindung interleaved sind
und so Head-Of-Line-Blocking vermeiden.
Streams können sowohl von Clients als auch Servern eröffnet werden,
sodass ein Server bereits proaktiv auf separaten Streams
weitere Daten, die vom Client möglicherweise benötigt werden, mitschicken kann.

HTTP/2 zeigt teilweise signifikante Verbesserungen,
vor allem bei Verbindungen mit hoher Latenz.
Dies ist bedingt dadurch, dass nur eine TCP-Verbindung hochfahren muss,
was schneller geht als mehrere Verbindungen hochzufahren.
Seiten mit geringer Latenz laden ähnlich schnell wie mit HTTP/1.1.
Der Speedup hängt also stark davon ab,
mit welcher Gegenstelle kommuniziert wird.
Head-Of-Line Blocking kann dennoch auftreten,
da TCP-Pakete verloren gehen können und damit zu Timeouts führen.
Insbesondere bei hohen Verlustraten tritt Head-Of-Line Blocking häufig auf.
Dies zeigt sich auch dadurch,
dass es starke Ausreißer ähnlich zu HTTP/1.1 in der Ladezeit von Webseiten gibt.

Es zeigt sich also, dass die Probleme eher auf der Transportschicht angesiedelt sind.

\section{QUIC}

\emph{QUIC}\index{QUIC} ist ein neues Transportprotokoll von Google,
um die Probleme von SPDY und HTTP/2 über TCP anzugehen.
Standardisiert ist es im RFC 9000\footnote{
	Ja, die haben extra mit Veröffentlichen gewartet,
	um eine schöne runde Zahl für die RFC-Nummer zu bekommen.
} \cite{iyengar_quic_2021}.
Ziele sind
\begin{itemize}
	\item Multi-Streaming ohne HoL-Blocking
	\item Multi-Homing
	\item Abwärtskompatibilität
	\item Built-In TLS
	\item Reduzierte Latenz
	\item Trennung der Congestion Control vom Transportprotokoll
	\item Forward Error Correction
		(zur Rekonstruktion verloren gegangener Pakete aus empfangenen Paketen)
\end{itemize}

QUIC arbeitet auf UDP\footnote{
	UDP wird allein für Abwärtskomaptibilität benutzt.
} und stellt darüber eine HTTP/2 API zur Verfügung.
Sessions werden mit einer 64 Bit ID identifiziert.
Es gibt zwei Arten von Headern.
Der lange Header hat einen Typen, Connection ID, Paketcounter und ein Versionsfeld.
Der kurze Header hat keinen Typen, sondern Flags
und außerdem keine Versionsnummer.
Zudem kann der Paketcounter 8, 16 oder 32 Bit groß sein.

Der TCP+TLS Verbindungsaufbau benötigt 6 Pakete,
um beide Handshakes auszuführen.
QUIC benutzt sog. \emph{0-RTT}\index{0-RTT} für den Verbindungsaufbau.
Dies bedeutet nicht, dass nur ein Paket verschickt wird,
sondern hält lediglich bereits bekannte Verbindungen auf unbestimmte Zeit offen,
sodass nach einer zuvor aufgebauten Verbindung (auch nach Tagen)
diese einfach weiterverwendet werden kann.

Tatsächlich wird zunächst einen normale HTTP-Verbindung aufgebaut.
Diese sendet eine Indication, dass QUIC unterstützt wird
sowie eine Wahrscheinlichkeit\footnote{
	Diese Wahrscheinlichkeit bietet die Möglichkeit,
	im Fehlerfall auf klassisches HTTP zu wechseln.
},
mit der der Client auf QUIC wechseln soll.
Beim QUIC-Handshake wird bereits im zweiten Paket das Zertifikat übertragen
und die verschlüsselte Anfrage kann bereits nach dem zweiten Paket
vom Client zum Server geschickt werden.

Da bereits im zweiten Paket das Zertifikat übertragen wird,
besteht theoretisch die Möglichkeit für Reflection-Angriffe.
Um dies zu verhindern, muss das erste Paket vom Client
mindestens so viel Padding enthalten,
dass das Paket groß genug wäre, um das Zertifikat zu speichern.

Die erste Anfrage vom Client wird bereits vor dem Schlüsselaustausch (Diffie-Hellman)
gestellt.
Damit ist Perfect Forward Secrecy nicht gegeben.
Dies kann je nach Art der Anfrage signifikante Sicherheitsprobleme zur Folge haben.
Außerdem wird durch das 0-RTT-Verfahren möglich,
durch geschicktes Delaying/Replaying von Paketen,
Clients zum Wiederholen von Anfragen zu bringen.
Replay Protection ist also nicht umgesetzt.

Ein Angriff bei TCP ist das Versenden von opportunistic ACKs,
also ACKs für Bytes, die noch nicht empfangen wurden.
Dies kann bei TCP architekturbedingt nicht verhindert werden
und erlaubt es, Server zum Versand mit sehr großer Senderate zu „überreden“.
QUIC hat einen Schutz dagegen,
indem es die Möglichkeit einräumt, Sequenznummern zu überspringen.
So wird es schwieriger für bösartige Clients,
die richtigen ACK-Nummern zu erraten.
