\chapter{Software Defined Networking}

Statt jetzt nur einzelne Netzknoten zu betrachten,
ist das Ziel von
\emph{Software Defined Networking}\index{Software Defined Networking}\index{SDN},
das Netz als ganzes so zu steuern, dass die Leistung optimiert wird.

\section{High-Level Motivation}

Bei der Entwicklung von Computern gab es einen Übergang
von Computern, die nur ein Programm ausführen,
hin zu Computern, die ein Betriebssystem haben
und über eine „offene“ Schnittstelle
die Ausführung mehrerer hardwareunabhängiger Programme erlauben.
Statt große spezialisierte Mainframes mit spezialisierter Hardware,
spezialisiertem Betriebssystem und spezialisierten Anwendungen zu betrieben,
sind heute viele General-Purpose-Rechner mit generischem Betriebssystem im Einsatz.

Bei Netzwerkhardware war lange auch der Standard,
dass eine spezialisierte Hardware ein spezialisiertes Betriebssystem hat,
auf dem spezialisierte Features laufen.
Die Systeme sind sehr komplex, geschlossen und proprietär,
ähnlich wie es auch auf früheren Mainframes üblich war.
Das macht es nicht nur schwer, Konkurrenz auf dem Markt zu etablieren
(was wiederum der Weiterentwicklung der Technik schadet),
sondern erlaubt auch viele „dreckige“ Hacks,
bei der die klare Schichtung zwischen Hardware, OS und Anwendung aufgebrochen wird,
um kleine Performance-Gewinne zu erzielen.

Stattdessen wäre ein Wunsch,
dass sich Router hin zu einer Schichtung ähnlich wie bei PCs entwickeln,
bei der proprietäre Switching-Chips über eine offene Schnittstellen
von einer Control Plane gesteuert werden,
die über eine offene Schnittstelle die Ausführung beliebiger Anwendungen
(z.\,B.~für eigene Routing-Protokolle) erlaubt.
Das würde es auch erlauben, die schnelle Entwicklung,
die mittlerweile in Rechenzentren stattfindet, gut angehen zu können.

\section{Archictecture}

Die Kernidee von SDN ist,
die eigentliche Logik, welche Pakete wie weitergeleitet werden,
auf einen externen Controller auszulagern,
in dem dann komplexere Entscheidungen als auf Switchen getroffen werden können.
Statt in der Control Plane jedes Switches OS und Dienste laufen zu lassen,
besteht die Control Plane nur noch aus einem Betriebssystem,
was Daten mit einem zentralen Server (dem \emph{Controller}\index{Controller!SDN})
austauscht.
Auf diesem laufen dann die Services, die die Switching-Entscheidungen treffen.

Der Controller kann dann den Switchen Regeln senden,
die bspw. Flows oder Header matchen und ihnen dann einen Output Port zuweisen
(oder auch andere Aktionen wie bspw. NAT durchzuführen).
Diese können in einer Flow-Tabelle gespeichert werden,
die im Data Patch schnell ausgewertet werden kann.
Es ist auch möglich, dass Switche den Controller darüber informieren,
wenn Pakete eintreffen, die noch keinen passenden Eintrag in der Flow-Tabelle haben,
um bspw. auf neue Flows reagieren zu können
oder auch bspw. ARP-Traffic vom Controller zu beantworten statt im Netzwerk zu fluten.

\todo{Lücke}

\section{CAP}

TL;DR Man kann höchsten zwei
von den Zielen Verfügbarkeit, Konsistenz und Partitionstoleranz erzielen.

Möglicherweise ist dies jedoch in der Realität etwas komplizierter.
Es ist natürlich klar,
dass man ohne Partitionierung Verfügbarkeit und Konsistenz sicherstellen kann.
Und wenn man gerade eine Partition hat,
kann man sich dennoch in jedem Schritt überlegen,
ob man lieber weiter Verfügbarkeit oder Konsistenz sicherstellen möchte.
Üblicherweise ist es eigentlich immer möglich,
Lesezugriffe weiter zuzulassen (bspw. DNS),
ohne Konsistenz zu verletzen.
In manchen Fällen ist es auch nicht nötig,
globale Konsistenz sicherzustellen (bspw. regional beschränkt bei EC-Kartenkäufen).

Eine bei Facebook übliche Konsistenz ist
\emph{Read-your-own-writes consistency}.
Dabei wird das System in ein Master- und mehrere Slave-Systeme unterteilt
und Clients sind mit Slave-Systemen verbunden.
Statt immer den Master instantan zu aktualisieren,
wird zunächst der eigene Slave aktualisiert und
der Master erhält die Änderungen erst später.

\subsection{Partition Recovery}

Dennoch lassen sich nicht alle Partitionen komplett mitigieren.
Daher müssen Strategien gefunden werden,
Partitionen zu erkennen und zu behandeln,
sodass Konsistenz später wiederhergestellt werden kann.

Es ist schon nicht immer klar zu erkennen,
ob eine Partition vorliegt (bspw.~wenn darunterliegende IP-Layer kaputt sind,
aber die Nodes sich indirekt noch sehen können).
Außerdem können die Knoten die Partition zu verschiedenen Zeitpunkten erkennen.
In diesen Fällen ist es anschließend notwenig,
später wieder Konsistenz herstellen zu können.
Dabei können Probleme bspw. bei der Erzeugung von IDs auftreten
(bspw. wenn beide Partitionen die gleichen IDs für verschiedene Objekte erzeugen).
Aber bei der Softwareentwicklung wird sich darauf verlassen,
dass bestimmte Invarianten in einem konsistenten System gelten,
bspw. dass solche automatisch erzeugten IDs immer eindeutig sind.
Im Fall einer Partition muss also Konsistenz wiederhergestellt werden,
bspw. indem Code die IDs nachträglich ändert.

Eine weitere Lösung sind konfliktfreie replizierte Datentypen (\emph{CRDT}s\index{CRDT}).
Beispielsweise serialisiert Google Docs Bearbeitungen in eine Liste
von Insert und Delete Operationen.
Hierbei können wieder Probleme auftreten,
bspw. wenn beide Partitionen jeweils andere Teile der Information ändern,
wobei das gemergte Ergebnis semantisch keinen Sinn mehr ergibt.

Ein generelles Problem bei Recovery ist aber weiter,
dass es aufwändig, fehleranfällig und selten getestet ist.
Prakisch strebt man immer irgendwie einen Kompromiss zwischen den Zielen an.
Bspw. könnte beim Ausfall der Verbindung zwischen Bankautomat und Bank
noch ein gewisses (sicheres) Limit an Geld abgehoben werden.

\subsection{CAP in SDN Infrastructures}

Während sich verteilte Systeme vor allem in höheren Layern bewegen,
ist SDN ein deutlich grundlegenderes System (Layer 2/3).
Da ein funktionierendes Netzwerk essentiell für den Betrieb
(und auch höhere Operationen) ist,
muss dass Netzwerk in der Lage sein,
sich selbst wiederherstellen zu können,
nachdem Fehler/Ausfälle aufgetreten sind.

Bei SDN gibt es keine globale Antwort auf das System.
Die Kernfrage ist dabei,
ob das Netzwerk auch noch funktioniert,
wenn das System nicht vollständig online ist.
Die nächste Frage ist, ob irgendwo zirkuläre Abhängigkeiten existieren,
die einen automatischen Kaltstart unmöglich machen.
So kann bspw. eine funktionierendes SDN erforderlich sein,
wenn die Steuerdaten des SDN in-band (also über das SDN) ausgetauscht werden.
Außerdem können Probleme enstehen,
wenn zwei Controller unterschiedliche Entscheidungen treffen,
die z.\,B.~zu Routing Loops führen können,
wenn zwei Controller für einen Flow zwei verschiedene Routen festlegen.

\section{OpenFlow}

\subsection{OpenFlow Components}
Die Kernidee von \cdefname{OpenFlow}\index{OpenFlow} ist
ein Switch mit Input Queue, Output Queue und Forwarding-Tabellen.
Statt jetzt jedoch die Forwarding-Tabelle mit Backward Learning zu managen,
wird über die sog. \emph{Southbound API Logic} mit einem OpenFlow-Controller gesprochen,
der die Forwarding-Tabellen managed.
In der Realität gibt es natürlich mehrere SDN-Controller,
die die Forwarding-Tabellen auf den Switchen aktualisieren können.
Der Management-Traffic zwischen Controllern und Switches läuft üblicherweise
in einem Out-of-Band-Netz.

Statt eine Flow-Tabelle pro Switch zu führen,
werden mehrere Flow-Tabellen kaskadiert.
Dies ermöglicht bessere Flexibilität bei der Steuerung des Switches.
Zusätzlich können Ports in Group Tables zusammengefasst werden,
um Weiterleitungen auf Gruppen von Ports zu ermöglichen
(bspw. für Multicast, Failover oder Load Balancing).
Außerdem wird noch zwischen physischen Ports (die tatsächlich and der Hardware existieren)
und logischen Ports (Repräsentatoren bspw. für VLANs, Tunnelprotokolle, LAG) unterschieden.
So können Regeln erstellt werden, die Pakete an logische Ports schicken,
um Traffic bspw. regelbasiert zu tunneln.
Weiterhin gibt es noch weitere reservierte Ports,
die Abstraktionen für bestimmte Aktionen im Switch
(bspw. Senden an alle Egress Ports, Start der OF-Pipeline,
Behandlung nach herkömmlicher Switching-Logik) repräsentieren.

Häufig haben Openflow-Switche auch eine traditionelle Switching-Logik,
um bspw. bestimmte Traffic-Klassen „normal“ zu behandeln (z.\,B.~Management-Traffic).

\subsection{OpenFlow Pipeline Processing}

Eingehende Pakete werden im Switch zunächst von Flow-Tabelle 0 bearbeitet werden,
in den Regeln kann jetzt das Paket an einen oder mehrere Output Ports geleitet,
das Paket manipuliert (z.\,B.~Headerfelder geändert)
oder auch die Auswertung weiterer Regeln durchgeführt werden.
Nach Ausführung aller Aktionen geht das Paket möglicherweise noch an eine Gruppentabelle
und wird an den Output Port geschickt,
wo wiederum Flow Tabellen Regeln ausführen können (sofern in Hardware verfügbar),
um zielspezifische Aktionen durchzuführen (z.\,B.~NAT).\footnote{
	Die Anforderung für OpenFlow-Switche ist nur das Vorhandensein \emph{einer}
	Flow-Tabelle im Ingress Processing.
	Es gibt nicht notwenigerweise mehrere Flow-Tabellen
	und auch nicht notwendigerweise Regelauswertung im Egress Processing.
}

Beim Matchen der Regeln werden immer alle Regeln einer Tabelle gematcht.
Die Regeln können verschiedene Prioritäten haben,
sodass die Regel mit höherer Priorität nach der Auswertung Vorrang bekommt.
Es ist nicht klar spezifiziert,
was passiert, wenn mehrere Regeln mit gleicher Priorität matchen.
Für den Fall, dass es kein Match gibt, lassen sich Policies festlegen.
Gematcht werden kann auf:
\begin{itemize}
	\item Port (Ingress/Egress)
	\item Ethernet-Adressen
	\item EtherType
	\item IP-Protokoll
	\item IP-Adressen
	\item TCP/UDP Ports
\end{itemize}
Regeln können haben:
\begin{itemize}
	\item Prioritäten
	\item Counter (werden beim Match hochgezählt, bspw. für statistische Erfassung)
	\item Instruktionen
	\item Timeouts (wann die Regel gelöscht werden soll)
	\item Cookies (Kennungen, bspw. um zu sehen, von wem die Regel ist)
	\item Flags (bspw. Benachrichtigung an den Controller, wenn eine Regel gelöscht wird)
\end{itemize}

Aktionen können sofort ausgeführt oder in einem Action Set
für spätere Ausführung vorgemerkt werden.
Letzteres ermöglicht es, bspw. einen Abbruch oder eine Weiterleitung an den Controller
durchzuführen,
wenn die zuvor ausgeführten Aktionen zu einem sinnlosen Ergebnis geführt haben.
Aktionen sind:
\begin{itemize}
	\item Pakete an bestimmte Ports schicken
	\item Pakete durch eine Gruppe verarbeiten
	\item Paket in eine Queue leiten
	\item Messen (z.\,B. für Traffic Shaping)
	\item Zusätzliche Tags (z.\,B. VLAN Header, MPLS-Label) hinzufügen oder entfernen
	\item Felder im Paket oder Metadaten setzen
	\item Felder kopieren
	\item TTLs ändern
	\item Pakete klonen
\end{itemize}

Counter können geführt werden für:
\begin{itemize}
	\item Flow-Tabellen
	\item Flow-Einträge
	\item Ports
	\item Queues
	\item Gruppen
	\item Group Buckets
	\item Meter
	\item Meter Band
\end{itemize}

Gruppentabellen haben wieder einen Identifier und Regeln,
wie Pakete verarbeitet werden sollen.
So können Pakete an jeden Bucket in der Gruppe,
Round Robin oder auch an den ersten verfügbaren Port geschickt werden,
um bspw. Broadcast/Multicast, Fast Failover oder auch Load Balancing zu realisieren.

Meter haben einen Identifier in einer Meter Table
und führen neben dem Counter auch Bands.
Diese sind ein Typ, haben eine Rate, bestimmte Counter (z.\,B. für Thresholds)
und typspezifische Argumente.
Diese können neben Queues für QoS und Traffic Shaping benutzt werden,
um bspw. eine maximale Bandbreite für bestimmte Trafficarten zuzulassen.

\subsection{Controller and Control Channel}

Zwischen Controller und Switch werden über einen Control Channel Nachrichten ausgetauscht.
Dieser Channel läuft auf TCP-Basis und kann optional mit TLS abgesichert werden.
Letzteres kann zwar Authentisierung und Vertraulichkeit sicherstellen,
führt aber zu Verfügbarkeitsproblemen,
da bspw. jetzt auch die Abhängigkeit zu funktionierenden RTCs und
gültigen Zertifikaten besteht,
die wiederum notwendig sein könnte,
um abgelaufene Zertifikate aktualisieren zu können.

Über diesen Control Channel werden Nachrichten ausgetauscht,
um Features der Switche zu ermitteln,
Konfigurationen zu lesen oder zu aktualisieren,
den aktuellen Zustand (Zählerwerte) auszulesen
oder auch Flow-Tabellen zu modifizieren.
Außerdem können Controller veranlassen,
dass Switche Pakete zu erzeugen bzw. ein vom Controller generiertes Paket zu senden.
So können bspw. DHCP-Anfragen zentral ohne Broadcasting beantwortet oder
LLDP-Nachrichten erzeugt werden.

OpenFlow ist ein nachrichtenorientes Protokoll auf TCP-Basis.
Daher kommen Nachrichten an Switchen in der richtigen Reihenfolge an.
Allerdings gibt es keine Garantien,
dass Befehle zu bestimmten Zeitpunkten schon fertig ausgeführt sind.
Wenn jedoch ein nachfolgender Befehl die Ausführung eines vorigen Befehls bedingt,
muss eine sogenannte \emph{Barrier}-Nachricht\index{Barrier},
bei der der Switch jetzt wartet,
bis alle Anfragen bearbeitet wurden.
Weiterhin können über den Channel Nachrichten zwischen Controllern ausgetauscht werden.

Switche können Pakete an Controller weiterleiten,
Controller über Änderungen an der Flow-Tabelle informieren,
Änderungen am Port Status anzeigen,
Controller über die Anmeldung eines neuen Master Controllers informieren
und auch den Zustand eines Flows an Controller weiterleiten.

Hinzu kommen synchrone Statusnachrichten
für Pings, Fehlermeldungen und andere Bookkeeping-Aufgaben.

\subsection{OpenFlow Switches}

Es gibt verschiedene Arten von Switches, die OpenFlow unterstützen.
Dazu gibt es zunächst die typischen Commodity Hardware Switche,
die von Herstellern angeboten werden und OpenFlow unterstützen.
Ein Problem bei diesen Switchen ist Performance,
da komplizierte Regeln auch viel Leistung zur Auswertung benötigen.
So kann es sein,
dass die Switche größer dimensioniert werden müssen
als das Netz eigentlich verarbeiten muss.
Für experientelle Aufbauten gibt es außerdem NetFPGA-basierte Hardware-Switches,
die sich umprogrammieren lassen.

Außerdem gibt es auch Software-Switches mit OpenFlow unterstützung.
Ein Beispiel hier für ist Open vSwitch.
Dies ermöglicht es,
VMs auf einem Server miteinander über virtuelle NICs miteinander zusammenzuschalten.
Statt im Linux Kernel viele Bridges zu verwalten,
wird ein Kernelmodul geladen,
was nur einfache Forwarding-Regeln für Flows anwendet,
ohne Pakete in den Userspace übertragen zu müssen.
Pakete ohne zutreffende Regel hingegen werden im Userspace-Daemon weitergeleitet.
Für die OpenFlow-Unterstützung,
läuft auf einem Open vSwitch ein Server,
der die Konfiguration hält und über ein Management-Protokoll
den Open vSwitch konfiguriert.

\section{SDN Controllers}

Das Ökosystem um SDN-Controller ist sehr divers.
Grundlegend haben diese jedoch immer als Schnittstellen
ein Southbound Interface,
um Switche zu konfigurieren (z.\,B. mit OpenFlow)
und ein Northbound Interface,
über welches Netzwerkanwendungen am Controller angebunden sind
und über die Dienste im System gesteuert werden können.
Für die Kommunikation zwischen Controllern
können Controller außerdem East-/Westbound Interfaces benutzt werden.

Im Controller laufen viele Module,
die verschiedene Logik ausführen,
um bspw. Flow-Regeln zu managen,
Statistiken regelmäßig auszulesen,
Sicherheitsmechanismen zu etablieren, etc.
Die genaue Liste an Modulen und Features ist jedoch immer davon abhängig,
welcher Controller benutzt wird.

\section{Scalability and Resilience in SDN}

Ein großes Probelem an SDN-Controllern ist,
dass diese erst einmal zentral sind.
Hauptargument ist,
dass die Controller nur logisch zentralisiert sind,
aber nicht physisch zentralisiert gebaut sein müssen.
Eine Idee ist, verteilte Controller zu bauen.
Das führt jedoch wieder zu weiteren Problemen.
Hauptproblem ist zunächst die Kommunikation zwischen Controllern.
Versuche, ein standardisiertes Protokoll für die East/Westbound-Kommunikation zu etablieren,
waren nicht erfolgreich,
da die Anwendungen im SDN Controller sehr divers sind.

Schaltet man mehrere SDN-Controller zusammen,
ist es möglicherweise auch wünschenswert,
dass die SDN Controller von einem anderen Controller zentral konfiguriert werden.

Zwischen autonomen System ist es auch möglich,
statt die Router BGP sprechen zu lassen,
könnten auch SDN-Controller der autonomen Systeme direkt miteinander verschaltet sein,
um BGP direkt auszutauschen.
So kann BGP als (stark eingeschränktes) East-/Westbound-Protokoll eingesetzt werden.
Die BGP-Verbindung wird dabei über die Router getunnelt.

Erste Ansätze, SDN im Netzwerk einzusetzen,
beruhten auf Ad hoc Entscheidungen,
bei denen Verbindungen erst reaktiv aufgebaut wurden.
Das hatte zu 30\,000 Anfragen pro Sekunde geführt,
was schnell zu Lastgrenzen geführt hat.
Moderne Implementierungen schaffen mittlerweile deutlich höhere Raten.
Dazu werden verschiedene Verfahren eingesetzt.

Ein erster Ansatz war, proaktiv Regeln für Flows einzutragen,
noch bevor Flows auftreten.
Weiter ist es möglich, Controller nah an Switches zu stellen,
um Latenzen zu minimieren.
Da Switch-CPUs bei SDN auch ein Bottleneck darstellen,
werden manche Switches mittlerweile auch mit leistungsfähigen CPUs ausgestattet
und die Pfade zum Controller direkt geswitcht statt über die CPU zu laufen.

Im Falle von Linkausfällen in einem echten (Layer 2) Netz
muss das Netzwerk weiterhin funktionieren.
In klassischen Netzen werden dazu Spanning-Tree-Protokolle eingesetzt,
die im Fall eines Linkausfalls einen neuen Spannbaum über das (noch funktionierende)
Netz berechnet.
Das ist recht langsam.
So lange jedoch in einem SDN-Netz die Verbindung zum Controller funktioniert,
kann der Controller über den Linkausfall informiert werden,
der dann die Flow-Regeln auf allen Switchen entsprechend anpasst.
Eine (schnellere) Alternative dazu ist aber auch,
ein Underlay-Netzwerk zu betreiben,
in dem alle Pakete geroutet werden und
in dem ein Routing-Protokoll auf Linkausfälle reagiert.

\subsection{Kandoo}

Bei verteilen SDNs mit mehrere Controllern wächst die Komplexität stark an.
Bei Ausfällen müssen Controller sich einig werden,
wie Entscheidungen getroffen werden.
Dafür gibt es \emph{Kandoo}\index{Kandoo},
eine Architektur mit zwei Hiearchieebenen von Controllern.
Switches reden mit lokalen Controllern und
treffen möglichst viele Entscheidungen lokal.
Nur „größere“ Entscheidungen werden vom Root Controller getroffen,
auf dem Nicht-lokale Applikationen laufen und Entscheidungen treffen.
Für Entwickler steigt hier wiederum die Komplexität dadurch,
dass entschieden werden muss, welche Anwendungen wo laufen sollen.
In manchen Einsatzfällen funktioniert dieser Ansatz,
aber insbesondere größere Netzwerkoptimierungen sind damit nicht möglich.
Beim Deployment solcher Setups stellt sich immer die Frage,
ob die Anzahl Probleme, die lokal gelöst werden können, signifikant ist.
Außerdem fragt sich, ob die Rechenleistung für die lokalen Controller
überhaupt an einem geeigneten Standort untergebracht werden kann.
Bei diesem Ansatz wurde allerdings auch nicht geklärt,
wie Konsistenz erzielt werden soll.
Außerdem leidet die Verfügbarkeit,
da der Ausfall des Root Controllers wieder dazu führt,
dass das SDN-Netz nicht richtig funktioniert.

\subsection{DevoFlow}

Im \emph{DevoFlow}\index{DevoFlow}-Paper wurde gemessen,
was physische Hardware kann und es wurde festgestellt,
dass eigentlich vor allem die Top Talker interessant waren.
Ziel war, OpenFlow so zuzubauen, dass solche Flows dies besser behandeln kann.

OpenFlow wurde im Paper um weitere Regeln erweitert.
Regeln können jetzt Wildcards enthalten und ein CLONE Flag besitzen,
welches dazu führt,
dass beim Match eine neue Regel erzeugt,
die besser matcht und mit der dann Counter geführt werden können.
So kann anhand der Regeln erzeugt werden, welche Flows die meisten Pakete senden.
Für Sampling wurden weiterhin Trigger eingebaut,
sodass mit einer gewissen Wahrscheinlichkeit
ein Paket eines Flows an den Controller geschickt wird.
Damit sollten Elephant-Flows am Controller erkannt und behandelt werden.

Durch Erkennung von Elephant Flows können diese dann
auf spezielle Pfaden im Netzwerk umgeleitet werden.

Mittels Netflow und IPFIX können jedoch auch Statistiken über Flows geführt werden.
Mit sFlow werden auch bereits Sample-basiert Paketdaten an zentrale Stellen geschickt,
um Elephant-Erkennung zu ermöglichen.
Diese Protokolle werden bereits heute von allen Switchen unterstützt,
was DevoFlow redundant erscheinen lässt.

Die Behandlung von Elephant Flows kann aber weitere Probleme erzeugen.
So können diese möglicherweise auf anderen Pfaden Flows verdrängen
oder Flapping verursachen.
Das kann zu chaotischem Verhalten im Netzwerk führen,
wodurch das Netz sehr instabil wird.

\section{Controller Placement}

Bisher nicht besprochen wurde die Frage,
wo SDN beim Einsatz über Weitverkehrsnetze
Controller platziert werden sollten.
Für Redundanz sollte es schon mindestens mehr als einen Controller geben.
Durch Latenzen in WANs gibt es zudem auch noch das Problem,
dass dann die Kommunikation zwischen Switchen und Controller viel Latenz hat.
Der Bedarf an Controllern und der Auslegung von Rechenzentren
wird bis heute vor allem nach Faustregeln geplant,
die auf Erfahrungen der Mitarbeiter beruhen.
Dennoch gibt es auch theoretische Forschung zu der Thematik mit dem Versuch,
objektive Kriterien zu finden,
wie die Controller ausgelegt werden sollten.

Ziele bei der Platzierung können bswp. die Minimierung
der durchschnittlichen Latenz und der Worst-Case-Latenz sein.
Das theoretische Optimum wäre hierbei natürlich immer,
an jedem Standort einen Controller zu platzieren.
Dem gegenüber stehen der zusätzliche Verwaltungsaufwand und die zusätzlichen Kosten.
So ist das Ziel,
möglichst geringe Latenzen zu erzielen,
ohne überall Controller zu platzieren.
Dieses Problem ist eine Variante des \emph{Facility-Location}-Problems\index{Facility Location},
welches NP-schwer ist.
Zu diesem Problem lassen sich auch Unterprobleme formulieren,
bspw. indem nur die durchschnittliche Latenz
zum nächstgelegenen Controller minimiert werden soll.
Ein solches Problem ist in P approximierbar mit konstanter Güte (\emph{APX}\index{APX}).
Solche Probleme sind zwar in NP,
lassen sich aber mit Greedy-Algorithmen in P approximieren.
In echten physischen Netzen können teilweise auch weitere Zusammenhänge
wie die Dreiecksungleichung angenommen werden
(nicht jedoch für das Internet).
Dann lassen sich wieder andere Verfahren benutzen,
die in APX sind, bspw. minimum \(k\)-center.

Eine weitere Problemklasse ist bspw. die Klasse der  Maximum Cover Probleme,
bei der möglichst wenige Controller platziert sollen,
sodass jeder Knoten entweder einen Controller hat
oder zu einem Knoten mit Controller benachbart ist.

Bei der Analyse des Internet2-Netzes wurden verschiedene solcher Algorithmen genutzt.
Je nach Anzahl Controller wurden Controller entweder im „Inneren“ des Netzes
oder am „Rand“ des Netzes platziert.
Außerdem hat sich gezeigt,
dass sich die durchschnittliche Latenz kaum verbessert,
wenn mehr Controller eingesetzt werden.
Die Worst-Case-Latenz hingegegen kann sich klar verbessern,
wenn mehr Controller eingesetzt werden.
Beim Einsatz von mehr Controllern übersteigen die Vorteile recht schnell die Kosten.

Hier zeigt sich klar,
dass die zufällige Auswahl von Standorten für Controller schlecht ist.
Manchmal ist es auch ausreichend, einen Controller einzusetzen.

\section{P4: Programmable Data Planes}

OpenFlow ist zwar nach wie vor der de facto Standard,
wird aber nicht mehr wirklich weiterentwickelt.
Die neuesten Standards haben mittlerweile extrem hohe Hardwareanforderungen,
sodass es nicht wirklich Hersteller gibt,
die aktuelle Versionen von OpenFlow einsetzen.

Dem gegenüber steht die starke Weiterentwicklung von Rechenzentren
zu Cloud-Architekturen.
In und zwischen Rechenzentren werden verschiedene Standards eingesetzt,
um Netze zu separieren (z.\,B. NVGRE, VxLAN, MPLS, TRILL).
Matcher in SDN-Switches müssen all diese Protokolle unterstützen.
Außerdem muss die Latenz zum Controller sehr gering gehalten werden.

Stattdessen wurde ein zu OpenFlow orthogonales Protokoll entwickelt,
um den Switch selbst programmieren können, nämlich \emph{P4}\index{P4}.
Dafür wurde eine eigene DSL entwickelt,
die zwar an C angelehnt ist,
aber deklarativer und ohne Schleifen geschrieben wurde.
Außerdem gibt es keine direkten Funktionalitäten,
um Zustand zu speichern.

P4 soll sich für verschiedene Plattformen wie ASICs, FPGAs und CPUs
kompilieren lassen.
Compiler sollen von Switch-Herstellern bereitgestellt werden.
Durch Schnittstellen soll es außerdem möglich sein,
auch zur Laufzeit die Konfiguration ähnlich zu OpenFlow zu ändern.

Entwickler schreiben mit einem vom Hersteller bereitgestellten Architekturmodell
ein P4-Programm,
was durch den Hersteller-Compiler in ein für den Switch geeignetes Binary kompiliert
und auf den Switch geladen werden.
In der Data Plane können so Tabellen gesteuert
bzw. das Verhalten der Control Plane beeinflusst werden.

P4-Programme bestehen immer aus drei Komponenten:
\begin{itemize}
	\item Parser durchlaufen State Machines für Pakete, ermitteln Header-Strukturen und
		akzeptieren ein Paket oder lehnen es ab.
	\item Eine \emph{Match-Action-Pipeline}\index{Match-Action-Pipeline},
		die das geparste Paket nach Regeln matcht
		und bei den passenden Regeln entsprechende Aktionen anwendet.
		Matches werden dabei vom Compiler in TCAM-Lookups übersetzt.
		Innerhalb der Match-Action-Pipeline können zwar Variablen gesetzt werden,
		aber diese können nicht über Pakete hinweg gespeichert werden.
	\item Der Deparser setzt die (möglicherweise veränderten) Komponenten
		wieder zu einem Paket zusammen,
		was dann verschickt oder erneut duch die Pipeline laufen kann.
\end{itemize}

Da nicht alle Funktionen so realisiert werden können (bspw. Berechnung von Checksummen),
gibt es sog. \emph{Externs}\index{Extern},
also vom Routerhersteller zu implementierende Erweiterungen des Codes,
die wie Funktionen aufgerufen werden können.

Vorteil von P4-Switchen ist,
dass SRAM/TCAM-Ressourcen viel feingranularer verteilt werden können.
Durch Software Updates lässt sich auch das Parsen von
zuvor unbekannten Protokollen umsetzen.

Allerdings gibt es nur wenig Hardware, die mit P4 programmierbar ist.
Forschung benutzt heute sehr häufig noch Softwareimplementierungen von P4,
bspw. Tofino.
Außerdem stellt sich die Frage, ob sich in Zukunft Protokolle noch stark ändern,
sodass sich eine Anpassung von P4-Code lohnen würde
bzw. ob die Änderungen, die eine Anpassung von P4-Code erfordern,
überhaupt möglich sind (ob bspw. die Externs der Hersteller dafür ausreichen).
Außerdem ist es auch schwierig zu sagen,
was „Line Speed“ bei einem P4-Switch bedeuten soll,
da insbesondere das mehrfache Durchlaufen der Paketverarbeitung
erfordern kann,
dass mehr Bandbreite im Switch benötigt wird,
als tatsächlich an Traffic durchgeleitet werden soll.
Außerdem tritt mit den Externs wieder das Problem auf,
dass Externs herstellerspezifisch sind
und entsprechend nicht garantiert werden kann,
dass bestimmte Features auf allen Switches verfügbar sind.

\section{White Box Switching}\index{White Box Switching}

Früher gab es die klare Schichtung zwischen
spezialisierter Forwarding Hardware, Betriebssystem und
darauf laufenden Apps.
„Gestern“ ar in Switchen fast überall der gleiche Chipsatz verbaut,
während nur noch das Betriebssystem vom Hersteller kommt.
Moderne Switche ermöglichen es,
über der spezialisierten Switching-Hardware
eigene Betriebssysteme (bspw. Cumulus Linux) mit eigenen Apps laufen zu lassen.
Dadurch verschieben sich die Grenzen der Programmierbarkeit
vom Southbound Interface des SDN-Controllers
zum Network OS auf der Control Plane des Switches.
Die Schnittstelle liegt jetzt als auf dem Switch selbst
zwischen dem Network OS und der Switching-Hardware.
Das erlaubt viel mehr Flexibilität.
Diese Architektur erlaubt es wieder,
Switche wie handelsübliche PCs mit herkömmlichen Programmiersprachen zu programmieren.

Das \emph{ONIE}\index{ONIE} ist eine Open Source Komponente,
um eigene Betriebssysteme auf Switchen zu bootstrappen.
Dies ist der de-facto Standard bei vielen Switch-Herstellern.
Mit einer Installationumgebung können
über Netzwerk oder USB-Sticks Betriebssysteme geladen werden.

Für die Konfiguration der Data Plane
steht das \emph{Switch Abstraction Interface} (SAI)\index{SAI} zur Verfügung,
mit dem alle wichtigen Aspekte der Switching Hardware
(z.\,B. Häufigkeit von LLDP-Nachrichten, MAC-Adresse, etc.) gesteuert werden können.
Das SAI selbst ist eine Sammlung von Headern
und die dazugehörige Library ist von den Herstellern zu implementieren.
